{
  "_id": "FiaLbtxlYdKfg0xi",
  "name": "Unknown",
  "type": "feat",
  "img": "systems/lf1/icons/skills/violet_07.jpg",
  "system": {
    "description": {
      "value": "<p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b> No<br><b>Usable with Summons</b> No<p>The unknown are terrifying fey creatures that feed off the mental energies of other creatures. Scholars studying the behavior and organization of the unknown speculate they may have originally been fey creatures that somehow reached the Dimension of Dreams and became trapped there, remade by other minds likewise lost in ephemeral dreamscapes. The unknown propagate more of their own kind by eroding humanoids’ psyches until the victims transform into new unknown. Unknown typically choose to victimize those who are already relatively helpless, especially children, but when they transform an accomplished adventurer into one of their own, the result can be truly terrifying.<p>“Unknown” is an acquired template for any humanoid. Unknown use the base creature abilities, except as noted.<p><b>Challenge Rating:</b> Base creature’s CR + 2.<p><b>Alignment:</b> Neutral evil.<p><b>Type:</b> The creature’s type changes to fey (augmented humanoid). Do not recalculate HD, BAB, or saves.<p><b>Senses:</b> An unknown gains low-light vision.<p><b>Armor Class:</b> Natural armor bonus improves by 2.<p><b>Defensive Abilities:</b> An unknown gains DR 10/cold iron.<p><b>Melee</b>: An unknown gains two claw attacks (assuming the base creature has two hands). These claws deal 1d6 points of damage if the unknown is Medium (1d4 if Small).<p><b>Special Attacks:</b> An unknown gains the following special attacks.<p><em>Psyche Erosion (Su):</em> Once per day, a target affected by an unknown’s victimize ability that sees the unknown’s true appearance must succeed at a Will save (DC = 10 + 1/2 the unknown’s total Hit Dice + the unknown’s Charisma modifier) or take 1d6 points of Charisma damage. A successful save negates the Charisma damage and also ends the unknown’s victimize effect on that target. As long as a creature is the target of an unknown’s victimize ability, it can’t recover the ability damage from psyche erosion, even through magic. If the Charisma damage from psyche erosion is equal to the target’s Charisma score, that creature doesn’t recover ability score damage naturally even if it ceases being the target of victimize. Only magic can fully heal this ability damage.<p>When its Charisma damage is equal to its Charisma score, the target falls into a nightmare-filled catatonia where it continues to be followed by an unknown. This dream state lasts for 1d4 days, and at the end, the character awakes if its Charisma damage has been reduced to less than its Charisma score. If not, the creature immediately loses all sense of self, becoming an unknown thrall. It replaces its Charisma score with the unknown’s Charisma score, and no longer takes Charisma damage from exposure to the unknown. Over time, typically 1 to 2 weeks, the thrall becomes a new unknown, gaining this template.<p>The unknown can share its senses with any of its thralls (even if it changes the target of its victimize ability), and as a full-round action, it can assume control of a thrall’s body, as per possessionOA. Typically, an unknown uses this ability to keep the thrall close by until it becomes an unknown. Unknowns often use thralls to lure new victims to its lair.<p>Psyche erosion is a mind-affecting effect. If your game uses the sanity system (see page 12), the erosion deals 2d6 points of sanity damage instead of Charisma damage, and triggers the catatonia and the transformation into a thrall if the victim’s sanity damage is equal to or exceeds the target’s sanity.<p><em>Victimize (Su):</em> As a swift action, an unknown can target a single creature within line of sight with this ability. After designating a target, the unknown can’t change the target of this ability for 24 hours, or until the target dies or succeeds at its save against the unknown’s psyche erosion ability. An unknown always knows the exact location of a victimized creature and the shortest route to reach it, even if it is on another plane (similar to a combined discern location and find the path); this is a divination effect and can only be prevented by mind blank and similar effects. A creature can be the target of only one unknown’s victimize ability at a time, and an unknown can victimize only one creature at a time.<p><b>Spell-Like Abilities:</b> An unknown can use blur, ghost sound, ventriloquism, and vocal alterationUM (self only) as spell-like abilities at will, with a caster level equal to its Hit Dice.<p><b>Languages:</b> An unknown gains Aklo as a bonus language. It can’t speak except by using its ventriloquism spell-like ability.<p><b>Special Qualities:</b> An unknown gains the following.<p><em>Assume Likeness (Su)</em>: As a standard action while using hallucinatory camouflage, an unknown is able to disguise itself as a specific creature known to the target of its victimize ability. If the victimized creature succeeds at a Will save (at the same DC as in psyche erosion) it negates this effect, and the unknown can’t use this ability again for 24 hours or until it changes victims, whichever comes first. Otherwise, the unknown’s features and mannerisms change to match the creature known to its victim, granting it a +20 circumstance bonus on Disguise checks to impersonate this creature. Unlike hallucinatory camouflage, this ability works against the target of victimize. This is a mind-affecting illusion (glamer) effect.<p><em>Dream Movements (Su): </em>To the unknown, all worlds are tinted with the shades of nightmares from the Dimension of Dreams, allowing them to defy the laws of physics as they move as if in a dream. An unknown gains an insight bonus to AC equal to its Charisma bonus. It can travel as if by dimension door as a move action at will, but only to enter or leave an area within line of sight of the target of its victimize ability. The victimized creature and the intended destination must both be within long range of the unknown.<p><em>Hallucinatory Camouflage (Su):</em> As long as an unknown is targeting a creature with its victimize ability, it hides its appearance behind a veil of illusions, appearing to be an unremarkable member of the victimized creature’s race to everyone except the target of victimize. A successful Will save (at the same DC as psyche erosion) allows a creature that interacts with the illusion to disbelieve the effect. This is a mind-affecting illusion (glamer) effect.<p><b>Ability Scores:</b> +2 Dexterity, +2 Constitution, +2 Intelligence, +4 Charisma.</p>"
    },
    "changes": [
      {
        "_id": "veukif7j",
        "formula": "2",
        "subTarget": "nac",
        "modifier": "untyped"
      },
      {
        "_id": "spbzkgv3",
        "formula": "2",
        "subTarget": "dex",
        "modifier": "untyped"
      },
      {
        "_id": "p72e0cug",
        "formula": "2",
        "subTarget": "con",
        "modifier": "untyped"
      },
      {
        "_id": "odmlm8qi",
        "formula": "2",
        "subTarget": "int",
        "modifier": "untyped"
      },
      {
        "_id": "ln8wjxv4",
        "formula": "4",
        "subTarget": "cha",
        "modifier": "untyped"
      }
    ],
    "subType": "template",
    "crOffset": "2"
  }
}
