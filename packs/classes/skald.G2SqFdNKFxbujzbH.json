{
  "_id": "G2SqFdNKFxbujzbH",
  "name": "Skald",
  "type": "class",
  "img": "systems/lf1/icons/items/inventory/drum.jpg",
  "system": {
    "description": {
      "value": "<p>Skalds are poets, historians, and keepers of lore who use their gifts for oration and song to inspire allies into a frenzied rage. They balance a violent spirit with the veneer of civilization, recording events such as heroic battles and the deeds of great leaders, enhancing these stories in the retelling to earn bloodier victories in combat. A skald’s poetry is nuanced and often has multiple overlapping meanings, and he applies similar talents to emulate magic from other spellcasters.</p>\n<p><b>Role</b>: A skald inspires his allies, and often presses forward to fight enemies in melee. Outside of combat, he’s useful as a healer and scholar, less versatile but more durable than a bard.</p>\n<p><b>Alignment</b>: Any.</p>\n<p><b>Hit Die</b>: d8.</p>\n<p><b>Parent Classes</b>: <em>Barbarian</em> and <em>bard</em>.</p>\n<p><b>Starting Wealth</b>: 3d6 × 10 gp (average 105 gp.) In addition, each character begins play with an outfit worth 10 gp or less.</p>\n<h2>Class Skills</h2>\n<p>The skald’s class skills are <em>Acrobatics</em> (<em>Dex</em>), <em>Appraise</em> (<em>Int</em>), <em>Bluff</em> (<em>Cha</em>), <em>Climb</em> (<em>Str</em>), <em>Craft</em> (<em>Int</em>), <em>Diplomacy</em> (<em>Cha</em>), <em>Escape Artist</em> (<em>Dex</em>), <em>Handle Animal</em> (<em>Cha</em>), <em>Intimidate</em> (<em>Cha</em>), <em>Knowledge</em> (all) (<em>Int</em>), <em>Linguistics</em> (<em>Int</em>), <em>Perception</em> (<em>Wis</em>), <em>Perform</em> (oratory, percussion, sing, string, wind<em>errata</em>) (<em>Cha</em>), <em>Profession</em> (<em>Wis</em>), <em>Ride</em> (<em>Dex</em>), <em>Sense Motive</em> (<em>Wis</em>), <em>Spellcraft</em> (<em>Int</em>), <em>Swim</em> (<em>Str</em>), and <em>Use Magic Device</em> (<em>Cha</em>).</p>\n<p><b>Skill Ranks per Level</b>: 4 + <em>Int</em> modifier.</p>"
    },
    "links": {
      "classAssociations": [
        {
          "name": "Bardic Knowledge (SKA)",
          "level": 1,
          "uuid": "Compendium.lf1.class-abilities.6789ez9tvie2bw08"
        },
        {
          "name": "Inspired Rage",
          "level": 1,
          "uuid": "Compendium.lf1.class-abilities.1HQUUhQhowqKfmCA"
        },
        {
          "name": "Raging Song",
          "level": 1,
          "uuid": "Compendium.lf1.class-abilities.sPqP7QFldieWEADi"
        },
        {
          "name": "Scribe Scroll",
          "level": 1,
          "uuid": "Compendium.lf1.class-abilities.BS9WDbCy2Up3QHw9"
        },
        {
          "name": "Versatile Performance",
          "level": 2,
          "uuid": "Compendium.lf1.class-abilities.HAqAsb5H56C6cZm3"
        },
        {
          "name": "Well-Versed",
          "level": 2,
          "uuid": "Compendium.lf1.class-abilities.wew6ophJrcab24m6"
        },
        {
          "name": "Rage Powers (SKA)",
          "level": 3,
          "uuid": "Compendium.lf1.class-abilities.i4GboHYSOofHEJ4i"
        },
        {
          "name": "Song of Marching",
          "level": 3,
          "uuid": "Compendium.lf1.class-abilities.jR8RFYhHsjRzzmB0"
        },
        {
          "name": "Uncanny Dodge",
          "level": 4,
          "uuid": "Compendium.lf1.class-abilities.7WaQxnVaaoL4AGr8"
        },
        {
          "name": "Spell Kenning",
          "level": 5,
          "uuid": "Compendium.lf1.class-abilities.tlvQVJUUFUVQ92EH"
        },
        {
          "name": "Song of Strength",
          "level": 6,
          "uuid": "Compendium.lf1.class-abilities.xqWYyijR7PxZGI69"
        },
        {
          "name": "Lore Master",
          "level": 7,
          "uuid": "Compendium.lf1.class-abilities.msQWK7TW7mufzf8l"
        },
        {
          "name": "Improved Uncanny Dodge",
          "level": 8,
          "uuid": "Compendium.lf1.class-abilities.ZfnHhhTFQVo0Lj4P"
        },
        {
          "name": "Damage Reduction (SKA)",
          "level": 9,
          "uuid": "Compendium.lf1.class-abilities.kMjMAG6Gjs7unAz5"
        },
        {
          "name": "Dirge of Doom",
          "level": 10,
          "uuid": "Compendium.lf1.class-abilities.pmWnRksUzrKvNOvy"
        },
        {
          "name": "Song of the Fallen",
          "level": 14,
          "uuid": "Compendium.lf1.class-abilities.CYOk7sUXkGbfRKPi"
        },
        {
          "name": "Master Skald",
          "level": 20,
          "uuid": "Compendium.lf1.class-abilities.q7aOR0jKvbkVHhyF"
        }
      ]
    },
    "tag": "skald",
    "useCustomTag": true,
    "armorProf": {
      "value": ["lgt", "med", "shl"]
    },
    "weaponProf": {
      "value": ["sim", "mar"]
    },
    "bab": "med",
    "skillsPerLevel": 4,
    "savingThrows": {
      "fort": {
        "value": "high"
      },
      "will": {
        "value": "high"
      }
    },
    "classSkills": {
      "acr": true,
      "apr": true,
      "art": true,
      "blf": true,
      "clm": true,
      "crf": true,
      "dip": true,
      "esc": true,
      "han": true,
      "int": true,
      "kar": true,
      "kdu": true,
      "ken": true,
      "kge": true,
      "khi": true,
      "klo": true,
      "kna": true,
      "kno": true,
      "kpl": true,
      "kre": true,
      "lin": true,
      "lor": true,
      "per": true,
      "prf": true,
      "pro": true,
      "rid": true,
      "sen": true,
      "spl": true,
      "swm": true,
      "umd": true
    },
    "casting": {
      "type": "spontaneous",
      "progression": "med",
      "ability": "cha",
      "spells": "arcane"
    }
  }
}
