{
  "_id": "16mfrGi2UHTsME4W",
  "name": "Shifter",
  "type": "class",
  "img": "systems/lf1/icons/skills/blue_34.jpg",
  "system": {
    "description": {
      "value": "<p>Whether riding on the wind as a falcon or hiding in some fetid bog waiting to strike, the shifter is a true master of the wild. Both a devoted defender of druidic circles and a fierce predator, the shifter can take on the forms of nature and even fuse them together with devastating effect and unbridled savagery. By way of the druidic discipline of <em>wild shape</em>, they become living aspects of the wild. At first, they are able to assume only a minor aspect, but with time and practice they can fully transform into ever more powerful forms.</p>\n<p>The shifter class offers players a way to experience a shapeshifting character that is more martially inclined than a spellcasting <em>druid</em>. With each new level, the shifter’s powers grow in new and surprising ways, creating a character that thrives in battle, exploration, and stealth.</p>\n<p>Shifters are protectors of druidic circles and avengers of nature, yet a shifter’s magic is different from that of her druidic kin. Rather than invoking spells from the natural world or forging alliances with <em>animals</em>, shifters focus their supernatural powers inward to gain control over their own forms. Their ability to change their forms is as varied as the wonders of the wilds themselves but always remains at least partially rooted in the natural world. There are many paths to becoming a shifter; most are trained in that role by druidic circles and have their powers unlocked via rituals of initiation. Yet some stumble upon the gift naturally, as if their blood bore the secrets of shifter transformation.</p>\n<p>For those leaning toward the causes of law and good, the path of the shifter is one of contemplation and understanding. They become one with nature through mental and physical mimicry and gain an ever deeper spiritual understanding of the ebb and flow of the natural world. Those leaning toward the chaotic and evil teachings of druidic philosophy find such enlightenment through more violent means. These are typically quicker transformations, both brutal and painful, imparting the dark lessons of nature through its most catastrophic forms. Shifters who lean toward true neutrality are the most diverse when it comes to their command of metamorphic secrets.</p>\n<p><b>Role</b>: The shifter is so attuned to nature and the wild beasts of the world that she can call upon those powers to mystically fortify her being. Fluid in form and function, she can shape herself to overcome hardships and support those she befriends or serves.</p>\n<p><b>Alignment</b>: Any neutral.</p>\n<p><b>Hit Die</b>: d10.</p>\n<p><b>Starting Wealth</b>: 3d6x10 gp (average 105 gp).</p>\n<h2>Class Skills</h2>\n<p>The shifter’s class skills are <em>Acrobatics</em> (<em>Dex</em>), <em>Climb</em> (<em>Str</em>), <em>Craft</em> (<em>Int</em>), <em>Fly</em> (<em>Dex</em>), <em>Handle Animal</em> (<em>Cha</em>), <em>Knowledge</em> (nature) (<em>Int</em>), <em>Perception</em> (<em>Wis</em>), <em>Profession</em> (<em>Wis</em>), <em>Ride</em> (<em>Dex</em>), <em>Stealth</em> (<em>Dex</em>), <em>Survival</em> (<em>Wis</em>), and <em>Swim</em> (<em>Str</em>).</p>\n<p><b>Skill Ranks per Level</b>: 4 + <em>Int</em> modifier.</p>"
    },
    "links": {
      "classAssociations": [
        {
          "name": "Shifter Aspect",
          "level": 1,
          "uuid": "Compendium.lf1.class-abilities.ph7qA9fJX3iPuqxa"
        },
        {
          "name": "Shifter Claws",
          "level": 1,
          "uuid": "Compendium.lf1.class-abilities.fi5PsRq1XW7MStPM"
        },
        {
          "name": "Wild Empathy",
          "level": 1,
          "uuid": "Compendium.lf1.class-abilities.Yr8dfM2d8JEWoYkr"
        },
        {
          "name": "Defensive Instinct",
          "level": 2,
          "uuid": "Compendium.lf1.class-abilities.l52geoeGQcQz84AE"
        },
        {
          "name": "Track",
          "level": 2,
          "uuid": "Compendium.lf1.class-abilities.r8wPs0bB3WhOLxbX"
        },
        {
          "name": "Woodland Stride",
          "level": 3,
          "uuid": "Compendium.lf1.class-abilities.5iXq1igb1Cq0qobT"
        },
        {
          "name": "Wild Shape (SHI)",
          "level": 4,
          "uuid": "Compendium.lf1.class-abilities.qjpLIatoQIzmcehq"
        },
        {
          "name": "Trackless Step",
          "level": 5,
          "uuid": "Compendium.lf1.class-abilities.adslhIPLFgIaHBfh"
        },
        {
          "name": "Shifter's Fury",
          "level": 6,
          "uuid": "Compendium.lf1.class-abilities.eM1RMYR1EUBuUVTv"
        },
        {
          "name": "Chimeric Aspect",
          "level": 9,
          "uuid": "Compendium.lf1.class-abilities.jPNCx02S3ZlIPOmL"
        },
        {
          "name": "Greater Chimeric Aspect",
          "level": 14,
          "uuid": "Compendium.lf1.class-abilities.bXxt8L2PITMstpka"
        },
        {
          "name": "A Thousand Faces",
          "level": 18,
          "uuid": "Compendium.lf1.class-abilities.cBwQdqQ4KmVBck3t"
        },
        {
          "name": "Timeless Body",
          "level": 18,
          "uuid": "Compendium.lf1.class-abilities.5JlthJkVGEHPZypG"
        },
        {
          "name": "Final Asepct",
          "level": 20,
          "uuid": "Compendium.lf1.class-abilities.joNziZgJl9ZOJZlF"
        }
      ]
    },
    "tag": "shifter",
    "useCustomTag": true,
    "armorProf": {
      "value": ["lgt", "med", "shl"],
      "custom": "No Metal Armor"
    },
    "weaponProf": {
      "custom": "Club;Dagger;Dart;Quarterstaff;Scimitar;Scythe;Sickle;Shortspear;Sling;Spear;Natural Attacks"
    },
    "hd": 10,
    "hp": 10,
    "bab": "high",
    "skillsPerLevel": 4,
    "savingThrows": {
      "fort": {
        "value": "high"
      },
      "ref": {
        "value": "high"
      }
    },
    "classSkills": {
      "acr": true,
      "art": true,
      "clm": true,
      "crf": true,
      "fly": true,
      "han": true,
      "kna": true,
      "lor": true,
      "per": true,
      "pro": true,
      "rid": true,
      "ste": true,
      "sur": true,
      "swm": true
    }
  }
}
