import { CheckboxFilter } from "./checkbox.mjs";

export class BuffTypeFilter extends CheckboxFilter {
  static label = "LF1.Type";
  static type = "buff";
  static indexField = "system.subType";

  /** @override */
  prepareChoices() {
    this.choices = this.constructor.getChoicesFromConfig(lf1.config.buffTypes);
  }
}
