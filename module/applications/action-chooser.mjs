import { getSkipActionPrompt } from "../documents/settings.mjs";

export class ActionChooser extends Application {
  /**
   * @param {ItemPF} item - The item for which to choose an attack
   * @param {any} [options]
   */
  constructor(item, options = {}) {
    super(options);

    this.item = item;
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      template: "systems/lf1/templates/apps/action-chooser.hbs",
      classes: ["lf1", "action-chooser"],
      width: 400,
    });
  }
  get title() {
    return game.i18n.format("LF1.Application.ActionChooser.Title", {
      actor: this.item.actor.name ?? "",
      item: this.item.name,
    });
  }

  async getData(options) {
    const result = await super.getData(options);

    result.item = this.item.toObject();
    result.actions = this.item.system.actions;

    return result;
  }

  activateListeners(html) {
    super.activateListeners(html);

    html.find(".action").on("click", this._onClickAction.bind(this));
  }

  _onClickAction(event) {
    event.preventDefault();

    const id = event.currentTarget.dataset?.action;
    this.item.use({ actionID: id, skipDialog: getSkipActionPrompt() });
    this.close();
  }
}
