export class Troubleshooter extends Application {
  get title() {
    return game.i18n.localize("LF1.Troubleshooter.Title");
  }

  get template() {
    return "systems/lf1/templates/apps/troubleshooter.hbs";
  }

  static get defaultOptions() {
    const options = super.defaultOptions;
    return {
      ...options,
      classes: [...options.classes, "lf1", "troubleshooter"],
      id: "lf1-troubleshooter",
      width: 460,
    };
  }

  getData() {
    return {
      isGM: game.user.isGM,
      links: {
        help: `<a data-action='help'>${game.i18n.localize("LF1.Troubleshooter.Steps.HelpLink")}</a>`,
        report: `<a href="https://gitlab.com/SimplyLinn/foundryvtt-lorefinder1/-/issues">${game.i18n.localize(
          "LF1.Troubleshooter.Steps.ReportLinkText"
        )}</a>`,
        faq: "https://gitlab.com/SimplyLinn/foundryvtt-lorefinder1/-/wikis/FAQs",
        helpmodule: `<a href="https://foundryvtt.com/packages/find-the-culprit">Find the Culprit</a>`,
      },
    };
  }

  /**
   * @param {Event} event
   */
  async _runMigration(event) {
    if (!game.user.isGM) return;

    /** @type {Element} */
    const el = event.target;
    el.classList.remove("finished");
    el.disabled = true;

    const target = el.dataset.target;
    switch (target) {
      case "world":
        await lf1.migrations.migrateWorld();
        break;
      case "modules":
        await lf1.migrations.migrateModules({ unlock: false });
        break;
      default:
        throw new Error(`Unrecognized migration target: "${target}"`);
    }

    el.disabled = false;
    el.classList.add("finished");
  }

  _openHelp(event) {
    lf1.applications.helpBrowser.openUrl("Help/Home");
  }

  /**
   * @param {JQuery} jq
   * @override
   */
  activateListeners(jq) {
    super.activateListeners(jq);

    const [html] = jq;

    const migrationButtons = html.querySelectorAll("button[data-action='migrate']");

    for (const button of migrationButtons) {
      button.addEventListener("click", this._runMigration.bind(this));
    }

    // React to external migration to minimal degree
    if (lf1.migrations.isMigrating) {
      for (const button of migrationButtons) {
        button.disabled = true;
      }

      Hooks.once("lf1MigrationFinished", () => {
        for (const button of migrationButtons) {
          button.disabled = false;
        }
      });
    }

    html.querySelector("a[data-action='help']").addEventListener("click", this._openHelp.bind(this));
  }

  static open() {
    new Troubleshooter().render(true, { focus: true });
  }
}
