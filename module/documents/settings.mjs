import { HealthConfig } from "../applications/settings/health.mjs";
import { ExperienceConfig } from "../applications/settings/experience.mjs";
import { AccessibilityConfig } from "../applications/settings/accessibility.mjs";
import { TooltipConfig } from "../applications/settings/tooltip.mjs";
import { TooltipWorldConfig } from "../applications/settings/tooltip_world.mjs";
import { TooltipPF } from "../applications/tooltip.mjs";
import { setDefaultSceneScaling } from "@utils";

export const registerSystemSettings = function () {
  /**
   * Track the system version upon which point a migration was last applied
   */
  game.settings.register("lf1", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: false,
    type: String,
    default: "0.0.0",
  });
  /**
   * Track when the last changelog was shown
   */
  game.settings.register("lf1", "changelogVersion", {
    name: "Changelog Version",
    scope: "client",
    config: false,
    type: String,
    default: "0.74.9",
  });
  game.settings.register("lf1", "dontShowChangelog", {
    name: "Don't Automatically Show Changelog",
    scope: "client",
    config: false,
    type: Boolean,
    default: false,
  });

  // Health configuration
  game.settings.registerMenu("lf1", "healthConfig", {
    name: "SETTINGS.lf1HealthConfigName",
    label: "SETTINGS.lf1HealthConfigLabel",
    hint: "SETTINGS.lf1HealthConfigHint",
    icon: "fas fa-heartbeat",
    type: HealthConfig,
    restricted: true,
  });
  game.settings.register("lf1", "healthConfig", {
    name: "SETTINGS.lf1HealthConfigName",
    scope: "world",
    default: HealthConfig.defaultSettings,
    type: Object,
    config: false,
    onChange: () => lf1.utils.refreshActors(),
  });

  // Experience configuration
  game.settings.registerMenu("lf1", "experienceConfig", {
    name: "LF1.ExperienceConfigName",
    label: "LF1.ExperienceConfigLabel",
    hint: "LF1.ExperienceConfigHint",
    icon: "fas fa-book",
    type: ExperienceConfig,
    restricted: true,
  });
  game.settings.register("lf1", "experienceConfig", {
    name: "LF1.ExperienceConfigName",
    scope: "world",
    default: ExperienceConfig.defaultSettings,
    type: Object,
    config: false,
    onChange: () => lf1.utils.refreshActors(),
  });

  // Accessibility configuration
  /*
  game.settings.registerMenu("lf1", "accessibilityConfig", {
    name: "LF1.AccessibilityConfigName",
    label: "LF1.AccessibilityConfigLabel",
    hint: "LF1.AccessibilityConfigHint",
    restricted: false,
    icon: "fas fa-wheelchair",
    type: AccessibilityConfig,
  });
  */
  game.settings.register("lf1", "accessibilityConfig", {
    name: "LF1.AccessibilityConfigName",
    scope: "client",
    default: AccessibilityConfig.defaultSettings,
    type: Object,
    config: false,
    onChange: () => lf1.utils.refreshActors(),
  });

  // Tooltip configuration
  game.settings.registerMenu("lf1", "tooltipConfig", {
    name: "LF1.TooltipConfigName",
    label: "LF1.TooltipConfigLabel",
    hint: "LF1.TooltipConfigHint",
    restricted: false,
    icon: "fas fa-window-maximize",
    type: TooltipConfig,
  });
  game.settings.register("lf1", "tooltipConfig", {
    name: "LF1.TooltipConfigName",
    scope: "client",
    default: TooltipConfig.defaultSettings,
    type: Object,
    config: false,
    onChange: (settings) => {
      const worldConf = game.settings.get("lf1", "tooltipWorldConfig");
      const enable = !worldConf.disabled && !settings.disabled;
      TooltipPF.toggle(enable);
    },
  });

  // Tooltip World configuration
  /* game.settings.registerMenu("lf1", "tooltipWorldConfig", {
    name: "LF1.TooltipWorldConfigName",
    label: "LF1.TooltipWorldConfigLabel",
    hint: "LF1.TooltipWorldConfigHint",
    restricted: true,
    icon: "fas fa-window-maximize",
    type: TooltipWorldConfig,
  }); */
  game.settings.register("lf1", "tooltipWorldConfig", {
    name: "LF1.TooltipWorldConfigName",
    scope: "world",
    default: TooltipWorldConfig.defaultSettings,
    type: Object,
    config: false,
    onChange: (settings) => {
      TooltipPF.toggle(!settings.disable);
      lf1.tooltip?.setPosition();
    },
  });

  // MEASURING

  /**
   * Option to change measure style
   */
  game.settings.register("lf1", "measureStyle", {
    name: "SETTINGS.lf1MeasureStyleN",
    hint: "SETTINGS.lf1MeasureStyleL",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
  });

  /**
   * Register diagonal movement rule setting
   */
  game.settings.register("lf1", "diagonalMovement", {
    name: "SETTINGS.lf1DiagN",
    hint: "SETTINGS.lf1DiagL",
    scope: "world",
    config: true,
    default: "5105",
    type: String,
    choices: {
      555: "SETTINGS.lf1DiagPHB",
      5105: "SETTINGS.lf1DiagDMG",
    },
    onChange: (rule) => (canvas.grid.diagonalRule = rule),
  });

  /**
   * System of Units
   */
  game.settings.register("lf1", "units", {
    name: "SETTINGS.lf1UnitsN",
    hint: "SETTINGS.lf1UnitsL",
    scope: "world",
    config: true,
    default: "imperial",
    type: String,
    choices: {
      imperial: game.i18n.localize("SETTINGS.lf1ImperialUnits"),
      metric: game.i18n.localize("SETTINGS.lf1MetricUnits"),
    },
    onChange: () => {
      lf1.utils.refreshActors();
      setDefaultSceneScaling();
    },
  });

  /**
   * System of units override for distances.
   */
  game.settings.register("lf1", "distanceUnits", {
    name: "SETTINGS.lf1DistanceUnitsN",
    hint: "SETTINGS.lf1DistanceUnitsL",
    scope: "world",
    config: true,
    default: "default",
    type: String,
    choices: {
      default: game.i18n.localize("LF1.Default"),
      imperial: game.i18n.localize("SETTINGS.lf1ImperialDistanceUnits"),
      metric: game.i18n.localize("SETTINGS.lf1MetricDistanceUnits"),
    },
    onChange: () => {
      lf1.utils.refreshActors({ renderOnly: true });
      setDefaultSceneScaling();
    },
  });

  /**
   * System of units override for weights.
   */
  game.settings.register("lf1", "weightUnits", {
    name: "SETTINGS.lf1WeightUnitsN",
    hint: "SETTINGS.lf1WeightUnitsL",
    scope: "world",
    config: true,
    default: "default",
    type: String,
    choices: {
      default: game.i18n.localize("LF1.Default"),
      imperial: game.i18n.localize("SETTINGS.lf1ImperialWeightUnits"),
      metric: game.i18n.localize("SETTINGS.lf1MetricWeightUnits"),
    },
    onChange: () => lf1.utils.refreshActors(),
  });

  // OPTIONAL RULES

  /**
   * Option to allow the background skills optional ruleset.
   */
  game.settings.register("lf1", "allowBackgroundSkills", {
    name: "SETTINGS.lf1BackgroundSkillsN",
    hint: "SETTINGS.lf1BackgroundSkillsH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => lf1.utils.refreshActors({ renderOnly: true }),
  });

  /**
   * Option to use the Fractional Base Bonuses optional ruleset.
   */
  game.settings.register("lf1", "useFractionalBaseBonuses", {
    name: "SETTINGS.lf1FractionalBaseBonusesN",
    hint: "SETTINGS.lf1FractionalBaseBonusesH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => lf1.utils.refreshActors(),
  });

  /**
   * Unchained action economy
   */
  game.settings.register("lf1", "unchainedActionEconomy", {
    name: "SETTINGS.lf1UnchainedActionEconomyN",
    hint: "SETTINGS.lf1UnchainedActionEconomyH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => lf1.utils.refreshActors({ renderOnly: true }),
  });

  // VISION

  /**
   * Low-light Vision Mode
   */
  game.settings.register("lf1", "lowLightVisionMode", {
    name: "SETTINGS.lf1LowLightVisionModeN",
    hint: "SETTINGS.lf1LowLightVisionModeH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => {
      // Refresh canvas sight
      canvas.perception.update(
        { initializeLighting: true, initializeVision: true, refreshLighting: true, refreshVision: true },
        true
      );
    },
  });

  /**
   * Shared Vision sharing style.
   */
  game.settings.register("lf1", "sharedVisionMode", {
    name: "SETTINGS.lf1SharedVisionModeN",
    hint: "SETTINGS.lf1SharedVisionModeH",
    scope: "world",
    config: false, // Hidden as it is unused; TODO: Re-implement #187's setting usage or remove setting/feature completely
    default: "0",
    type: String,
    choices: {
      0: "SETTINGS.lf1SharedVisionWithoutSelection",
      1: "SETTINGS.lf1SharedVisionWithSelection",
    },
    onChange: () => canvas.perception.update({ refreshLighting: true, refreshVision: true }, true),
  });

  /**
   * Enable vision for player characters by default.
   */
  game.settings.register("lf1", "characterVision", {
    name: "SETTINGS.lf1characterVisionN",
    hint: "SETTINGS.lf1characterVisionH",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
  });

  // CHAT CARDS

  /**
   * Option to automatically collapse Item Card descriptions
   */
  game.settings.register("lf1", "autoCollapseItemCards", {
    name: "SETTINGS.lf1AutoCollapseCardN",
    hint: "SETTINGS.lf1AutoCollapseCardL",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => ui.chat.render(),
  });

  /**
   * Option to hide chat buttons
   */
  game.settings.register("lf1", "hideChatButtons", {
    name: "SETTINGS.lf1HideChatButtonsN",
    hint: "SETTINGS.lf1HideChatButtonsH",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => ui.chat.render(),
  });

  // HOMEBREW

  /**
   * Set coin weight
   */
  game.settings.register("lf1", "coinWeight", {
    name: "SETTINGS.lf1CoinWeightN",
    hint: "SETTINGS.lf1CoinWeightH",
    scope: "world",
    config: true,
    default: 50,
    type: Number,
    onChange: () => lf1.utils.refreshActors(),
  });

  /**
   * Default spellpoint cost
   */
  game.settings.register("lf1", "spellPointCost", {
    name: "SETTINGS.lf1SpellPointCostN",
    hint: "SETTINGS.lf1SpellPointCostH",
    scope: "world",
    config: true,
    default: "1 + @sl",
    type: String,
    onChange: () => lf1.utils.refreshSheets({ reset: false }),
  });

  /**
   * Alternative reach corner rule
   */
  game.settings.register("lf1", "alternativeReachCornerRule", {
    name: "SETTINGS.lf1AlternativeReachCornerRuleN",
    hint: "SETTINGS.lf1AlternativeReachCornerRuleH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
  });

  /**
   * Allow proficiencies on NPCs.
   */
  game.settings.register("lf1", "npcProficiencies", {
    name: "SETTINGS.lf1NPCProficienciesN",
    hint: "SETTINGS.lf1NPCProficienciesH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
  });

  // TOKENS / CONDITIONS

  /**
   * Display default token conditions alongside system ones
   */
  game.settings.register("lf1", "coreEffects", {
    name: "SETTINGS.lf1CoreEffectsN",
    hint: "SETTINGS.lf1CoreEffectsH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    requiresReload: true,
  });

  /**
   * Hide token conditions
   */
  game.settings.register("lf1", "hideTokenConditions", {
    name: "SETTINGS.lf1HideTokenConditionsN",
    hint: "SETTINGS.lf1HideTokenConditionsH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => {
      if (!game.users.activeGM.isSelf) return;

      const promises = [];
      const actors = [
        ...game.actors.filter((actor) => actor.prototypeToken.actorLink),
        ...Object.values(game.actors.tokens).filter((actor) => actor != null),
      ];
      for (const actor of actors) {
        if (actor.toggleConditionStatusIcons) {
          promises.push(actor.toggleConditionStatusIcons({ render: false }));
        }
      }
      return Promise.all(promises);
    },
  });

  // TRANSPARENCY

  /**
   * Hide inline rolls from non-observers.
   */
  game.settings.register("lf1", "obscureInlineRolls", {
    name: "SETTINGS.lf1obscureInlineRollsN",
    hint: "SETTINGS.lf1obscureInlineRollsH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    requiresReload: true,
  });

  /**
   * Hide save DCs.
   */
  game.settings.register("lf1", "obscureSaveDCs", {
    name: "SETTINGS.lf1obscureSaveDCsN",
    hint: "SETTINGS.lf1obscureSaveDCsH",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    requiresReload: true,
  });

  // COMBAT

  game.settings.register("lf1", "initiativeTiebreaker", {
    name: "SETTINGS.lf1InitTiebreakerN",
    hint: "SETTINGS.lf1InitTiebreakerH",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    requiresReload: true,
  });

  // USER INTERFACE

  /**
   * Skip action dialog prompts
   */
  game.settings.register("lf1", "skipActionDialogs", {
    name: "SETTINGS.lf1SkipActionDialogsN",
    hint: "SETTINGS.lf1SkipActionDialogsH",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
  });

  /*
   * When skipping an action dialog prompt still place the template if one is configured
   */
  game.settings.register("lf1", "placeMeasureTemplateOnQuickRolls", {
    name: "SETTINGS.placeMeasureTemplateOnQuickRollsN",
    hint: "SETTINGS.placeMeasureTemplateOnQuickRollsH",
    scope: "client",
    config: true,
    default: true,
    type: Boolean,
  });

  /**
   * Invert filter Shift-clicking
   */
  game.settings.register("lf1", "invertSectionFilterShiftBehaviour", {
    name: "SETTINGS.lf1InvertSectionFilterBehaviourN",
    hint: "SETTINGS.lf1InvertSectionFilterBehaviourH",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
  });

  /**
   * Hide reach measurements
   */
  game.settings.register("lf1", "hideReachMeasurements", {
    name: "SETTINGS.lf1HideReachMeasurementsN",
    hint: "SETTINGS.lf1HideReachMeasurementsH",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
  });

  /**
   * Display BAB iteratives instead of simply total
   */
  game.settings.register("lf1", "displayIteratives", {
    name: "SETTINGS.lf1DisplayIterativesN",
    hint: "SETTINGS.lf1DisplayIterativesH",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
  });

  // TARGETING

  /**
   * Disable targets for attack cards
   */
  game.settings.register("lf1", "disableAttackCardTargets", {
    name: "SETTINGS.lf1DisableAttackCardTargetsN",
    hint: "SETTINGS.lf1DisableAttackCardTargetsH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
  });

  /**
   * Clear targets after attack
   */
  game.settings.register("lf1", "clearTargetsAfterAttack", {
    name: "SETTINGS.lf1ClearTargetsAfterAttackN",
    hint: "SETTINGS.lf1ClearTargetsAfterAttackH",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
  });

  // SECURITY

  /**
   * Allow Script type Changes.
   */
  game.settings.register("lf1", "allowScriptChanges", {
    name: "SETTINGS.lf1AllowScriptChangesN",
    hint: "SETTINGS.lf1AllowScriptChangesH",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: (value) => {
      if (!value || !game.user.isGM) return;
      // Flash scare message and confirmation
      const d = Dialog.confirm({
        title: game.i18n.localize("SETTINGS.lf1AllowScriptChangesN"),
        content: game.i18n.localize("SETTINGS.lf1AllowScriptChangesW"),
        defaultYes: false,
      });
      d.then((result) => {
        if (!result) game.settings.set("lf1", "allowScriptChanges", false);
      });
    },
  });
};

export const registerClientSettings = function () {};

export const migrateSystemSettings = async function () {
  // Delete now unused compendium browser cache
  game.settings.storage.get("client").removeItem("lf1.compendiumItems");

  if (!game.user.isGM) return;

  // Currently empty, since the last option was removed (2022-06-06)
};

/**
 * Returns whether the user's settings and key presses signal that dialogs should be skipped.
 *
 * @returns {boolean}
 */
export const getSkipActionPrompt = function () {
  return (
    (game.settings.get("lf1", "skipActionDialogs") && !lf1.skipConfirmPrompt) ||
    (!game.settings.get("lf1", "skipActionDialogs") && lf1.skipConfirmPrompt)
  );
};
