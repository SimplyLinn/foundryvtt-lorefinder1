/**
 * Transforms a key input into an array of objects for the keybinding API
 *
 * @param {string} key - A key string
 * @returns {{"key": string}[]} Keybinding objects
 */
const getLeftRight = (key) => [`${key}Left`, `${key}Right`].map((k) => ({ key: k }));

const SHIFT_KEYS = getLeftRight("Shift");
const CTRL_KEYS = getLeftRight("Control");

/**
 * Registers the system's keybindings
 */
export const registerSystemControls = () => {
  game.keybindings.register("lf1", "skipConfirmPrompt", {
    name: "LF1.KEYBINDINGS.SkipConfirmPrompt.Name",
    uneditable: SHIFT_KEYS,
    onDown: () => {
      lf1.skipConfirmPrompt = true;
    },
    onUp: () => {
      lf1.skipConfirmPrompt = false;
    },
  });

  game.keybindings.register("lf1", "forceShowItem", {
    name: "LF1.KEYBINDINGS.ForceShowItem.Name",
    hint: game.i18n.localize("LF1.KEYBINDINGS.ForceShowItem.Hint"),
    uneditable: CTRL_KEYS,
    onDown: () => {
      lf1.forceShowItem = true;
    },
    onUp: () => {
      lf1.forceShowItem = false;
    },
  });

  game.keybindings.register("lf1", "hideTokenTooltip", {
    name: "LF1.KEYBINDINGS.HideTokenTooltip.Name",
    hint: game.i18n.localize("LF1.KEYBINDINGS.HideTokenTooltip.Hint"),
    uneditable: CTRL_KEYS,
    onDown: () => lf1.documents.controls._hideTokenTooltip(true),
    onUp: () => lf1.documents.controls._hideTokenTooltip(false),
  });

  game.keybindings.register("lf1", "hideTokenTooltipGMInfo", {
    name: "LF1.KEYBINDINGS.HideTokenTooltipGMInfo.Name",
    uneditable: SHIFT_KEYS,
    restricted: true,
    onDown: () => lf1.documents.controls._hideTokenTooltipGMInfo(true),
    onUp: () => lf1.documents.controls._hideTokenTooltipGMInfo(false),
  });
};

/**
 * Toggle the display of GM/Player info in the token tooltip
 *
 * @param {boolean} keyDown - Whether the key is pressed down
 * @returns {Promise<void>|void} A Promise that is resolved when the tooltip render handling is done
 */
export const _hideTokenTooltipGMInfo = (keyDown) => {
  if (!lf1.tooltip) return;
  lf1.tooltip.forceHideGMInfo = keyDown;
  return lf1.tooltip?.refresh();
};

/**
 * Toggle the display of the token tooltip
 *
 * @param {boolean} keyDown - Whether the key is pressed down
 * @returns {Promise<void>|void} A Promise that is resolved when the tooltip render handling is done
 */
export const _hideTokenTooltip = (keyDown) => {
  if (!lf1.tooltip) return;
  if (game.settings.get("lf1", "tooltipConfig")?.hideWithoutKey === true) lf1.tooltip.forceHide = !keyDown;
  else lf1.tooltip.forceHide = keyDown;
  return lf1.tooltip?.refresh();
};
