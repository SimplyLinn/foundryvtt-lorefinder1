import { Registry, RegistryEntry } from "./base-registry.mjs";

const fields = foundry.data.fields;

/**
 * A single script call category/trigger.
 *
 * @group Script Call Categories
 */
export class ScriptCallCategory extends RegistryEntry {
  static defineSchema() {
    return {
      ...super.defineSchema(),
      itemTypes: new fields.ArrayField(new fields.StringField({})),
      info: new fields.StringField({ required: true, blank: false, initial: "" }, { localize: true }),
    };
  }
}

/**
 * The singleton registry of script call categories/trigger events.
 * At runtime this registry is accessible as `lf1.registry.scriptCalls`.
 *
 * @group Script Call Categories
 * @see {@link Registry}
 * @see {@link ScriptCallCategory}
 * @augments {Registry<ScriptCallCategory>}
 */
export class ScriptCalls extends Registry {
  /** @inheritdoc */
  static model = ScriptCallCategory;

  /** @inheritdoc */
  static _defaultData = [
    // Use
    {
      _id: "use",
      itemTypes: ["attack", "buff", "feat", "equipment", "consumable", "spell", "weapon"],
      name: "LF1.ScriptCalls.Use.Name",
      info: "LF1.ScriptCalls.Use.Info",
    },
    // Equip
    {
      _id: "equip",
      itemTypes: ["weapon", "equipment", "loot"],
      name: "LF1.ScriptCalls.Equip.Name",
      info: "LF1.ScriptCalls.Equip.Info",
    },
    // Toggle
    {
      _id: "toggle",
      itemTypes: ["buff", "feat"],
      name: "LF1.ScriptCalls.Toggle.Name",
      info: "LF1.ScriptCalls.Toggle.Info",
    },
    // Change Quantity
    {
      _id: "changeQuantity",
      itemTypes: ["loot", "equipment", "weapon", "consumable", "container"],
      name: "LF1.ScriptCalls.ChangeQuantity.Name",
      info: "LF1.ScriptCalls.ChangeQuantity.Info",
    },
    // Change Level
    {
      _id: "changeLevel",
      itemTypes: ["buff", "class"],
      name: "LF1.ScriptCalls.ChangeLevel.Name",
      info: "LF1.ScriptCalls.ChangeLevel.Info",
    },
  ];
}

/**
 * {@inheritDoc ScriptCalls}
 *
 * @group Script Call Categories
 * @type {ScriptCalls}
 */
export let scriptCalls;
