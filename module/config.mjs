/**
 * LF1 Configuration Values
 *
 * A dictionary of dictionaries providing configuration data like formulae,
 * translation keys, and other configuration values. Translations keys are
 * assumed to get replaced by their proper translation when the system is loaded.
 *
 * The LF1 object may be adjusted to influence the system's behaviour during runtime.
 * It is available as `lf1.config` as well as through `CONFIG.LF1`.
 *
 * @module
 */
export const re = {
  traitSeparator: /\s*[;]\s*/g,
};

/**
 * The set of Ability Scores used within the system
 *
 * @enum {string}
 */
export const abilities = {
  str: "LF1.AbilityStr",
  dex: "LF1.AbilityDex",
  con: "LF1.AbilityCon",
  int: "LF1.AbilityInt",
  wis: "LF1.AbilityWis",
  cha: "LF1.AbilityCha",
};

/**
 * The set of Ability Scores used within the system in short form
 */
export const abilitiesShort = {
  str: "LF1.AbilityShortStr",
  dex: "LF1.AbilityShortDex",
  con: "LF1.AbilityShortCon",
  int: "LF1.AbilityShortInt",
  wis: "LF1.AbilityShortWis",
  cha: "LF1.AbilityShortCha",
};

/**
 * The point cost to increase an ability score using Point Buy
 */
export const abilityCost = {
  7: -4,
  8: -2,
  9: -1,
  10: 0,
  11: 1,
  12: 2,
  13: 3,
  14: 5,
  15: 7,
  16: 10,
  17: 13,
  18: 17,
};

/**
 * Point buy calculator.
 */
export const pointBuy = {
  low: { label: "LF1.PointBuyCalculatorLowFantasy", points: 10 },
  standard: { label: "LF1.PointBuyCalculatorStandardFantasy", points: 15 },
  high: { label: "LF1.PointBuyCalculatorHighFantasy", points: 20 },
  epic: { label: "LF1.PointBuyCalculatorEpicFantasy", points: 25 },
};

/**
 * At which levels you receive how many new ability points.
 */
export const levelAbilityScores = {
  4: 1,
  8: 1,
  12: 1,
  16: 1,
  20: 1,
};

/**
 * Data for the feature associated with ability scores gained from leveling.
 */
export const levelAbilityScoreFeature = {
  img: "systems/lf1/icons/skills/affliction_10.jpg",
  name: "LF1.LevelUp.AbilityScore.Item.Name",
  system: {
    description: {
      value: "LF1.LevelUp.AbilityScore.Item.Desc",
    },
    subType: "misc",
  },
  type: "feat",
};

/**
 * The set of Saving Throws
 */
export const savingThrows = {
  fort: "LF1.SavingThrowFort",
  ref: "LF1.SavingThrowRef",
  will: "LF1.SavingThrowWill",
};

/**
 * The types of classes
 */
export const classTypes = {
  base: "LF1.ClassTypeBase",
  prestige: "LF1.ClassTypePrestige",
  npc: "LF1.ClassTypeNPC",
  racial: "LF1.ClassTypeRacial",
  mythic: "LF1.ClassTypeMythic",
};

/**
 * Valid options for a class's BAB progression
 */
export const classBAB = {
  low: "LF1.Low",
  med: "LF1.Medium",
  high: "LF1.High",
  custom: "LF1.Custom",
};

/**
 * Valid options for a class's saving throw bonus progression
 */
export const classSavingThrows = {
  low: "LF1.Poor",
  high: "LF1.Good",
  custom: "LF1.Custom",
};

/**
 * The formulae for BAB progressions
 */
export const classBABFormulas = {
  low: "floor(@hitDice * 0.5)",
  med: "floor(@hitDice * 0.75)",
  high: "@hitDice",
  custom: "0",
};

export const classFractionalBABFormulas = {
  low: "@hitDice * 0.5", // 1/2
  med: "@hitDice * 0.75", // 3/4
  high: "@hitDice", // 1/1
  custom: "0",
};

/**
 * The formulae for saving throw progressions by class type
 */
export const classSavingThrowFormulas = {
  base: {
    low: "floor(@hitDice / 3)",
    high: "2 + floor(@hitDice / 2)",
  },
  prestige: {
    low: "floor((1 + @hitDice) / 3)",
    high: "floor((1 + @hitDice) / 2)",
  },
  npc: {
    low: "floor(@hitDice / 3)",
    high: "2 + floor(@hitDice / 2)",
  },
  racial: {
    low: "floor(@hitDice / 3)",
    high: "2 + floor(@hitDice / 2)",
  },
  mythic: {
    low: "0",
    high: "0",
  },
  custom: {
    low: "0",
    high: "0",
  },
};
export const classFractionalSavingThrowFormulas = {
  goodSaveBonus: "2",
  base: {
    low: "@hitDice / 3",
    high: "@hitDice / 2",
    goodSave: true,
  },
  prestige: {
    low: "(1 + @hitDice) / 3",
    high: "(1 + @hitDice) / 2",
  },
  npc: {
    low: "@hitDice / 3",
    high: "@hitDice / 2",
    goodSave: true,
  },
  racial: {
    low: "@hitDice / 3",
    high: "@hitDice / 2",
    goodSave: true,
  },
  mythic: {
    low: "0",
    high: "0",
  },
  custom: {
    low: "0",
    high: "0",
  },
};

/**
 * The choices available for favored class bonuses
 */
export const favouredClassBonuses = {
  hp: "LF1.FavouredClassHP",
  skill: "LF1.FavouredClassSkill",
  alt: "LF1.FavouredClassAlt",
};

/**
 * Icons used for favored class bonus choices
 */
export const favouredClassBonusIcons = {
  hp: "fa-heartbeat",
  skill: "fa-wrench",
  alt: "fa-tag",
};

/**
 * The set of Armor Classes
 */
export const ac = {
  normal: "LF1.ACNormal",
  touch: "LF1.ACTouch",
  flatFooted: "LF1.ACFlatFooted",
};

/* -------------------------------------------- */

/**
 * Character alignment options
 */
export const alignments = {
  lg: "LF1.AlignmentLG",
  ng: "LF1.AlignmentNG",
  cg: "LF1.AlignmentCG",
  ln: "LF1.AlignmentLN",
  tn: "LF1.AlignmentTN",
  cn: "LF1.AlignmentCN",
  le: "LF1.AlignmentLE",
  ne: "LF1.AlignmentNE",
  ce: "LF1.AlignmentCE",
};

/**
 * Character alignment options in their short form
 */
export const alignmentsShort = {
  lg: "LF1.AlignmentShortLG",
  ng: "LF1.AlignmentShortNG",
  cg: "LF1.AlignmentShortCG",
  ln: "LF1.AlignmentShortLN",
  tn: "LF1.AlignmentShortTN",
  cn: "LF1.AlignmentShortCN",
  le: "LF1.AlignmentShortLE",
  ne: "LF1.AlignmentShortNE",
  ce: "LF1.AlignmentShortCE",
};

/* -------------------------------------------- */

/**
 * The set of Armor Proficiencies which a character may have
 */
export const armorProficiencies = {
  lgt: "LF1.ArmorProfLight",
  med: "LF1.ArmorProfMedium",
  hvy: "LF1.ArmorProfHeavy",
  shl: "LF1.ArmorProfShield",
  twr: "LF1.ArmorProfTowerShield",
};

/**
 * The set of broad Weapon Proficiencies a character may have
 */
export const weaponProficiencies = {
  sim: "LF1.WeaponProfSimple",
  mar: "LF1.WeaponProfMartial",
};

/* -------------------------------------------- */

/**
 * This describes the ways that an ability can be activated.
 */
export const abilityActivationTypes = {
  passive: "LF1.ActivationTypePassive",
  free: "LF1.ActivationTypeFree",
  nonaction: "LF1.ActivationTypeNonaction",
  swift: "LF1.ActivationTypeSwift",
  immediate: "LF1.ActivationTypeImmediate",
  move: "LF1.ActivationTypeMove",
  standard: "LF1.ActivationTypeStandard",
  full: "LF1.ActivationTypeFullround",
  attack: "LF1.ActivationTypeAttack",
  aoo: "LF1.ActivationTypeAoO",
  round: "LF1.ActivationTypeRound",
  minute: "LF1.ActivationTypeMinute",
  hour: "LF1.ActivationTypeHour",
  special: "LF1.ActivationTypeSpecial",
};

/**
 * This describes plurals for activation types.
 */
export const abilityActivationTypesPlurals = {
  free: "LF1.ActivationTypeFreePlural",
  swift: "LF1.ActivationTypeSwiftPlural",
  immediate: "LF1.ActivationTypeImmediatePlural",
  move: "LF1.ActivationTypeMovePlural",
  standard: "LF1.ActivationTypeStandardPlural",
  full: "LF1.ActivationTypeFullroundPlural",
  attack: "LF1.ActivationTypeAttackPlural",
  round: "LF1.ActivationTypeRoundPlural",
  minute: "LF1.ActivationTypeMinutePlural",
  hour: "LF1.ActivationTypeHourPlural",
};

/**
 * This describes the ways that an ability can be activated when using
 * Unchained rules.
 */
export const abilityActivationTypes_unchained = {
  passive: "LF1.ActivationTypePassive",
  free: "LF1.ActivationTypeFree",
  nonaction: "LF1.ActivationTypeNonaction",
  reaction: "LF1.ActivationTypeReaction",
  action: "LF1.ActivationTypeAction",
  attack: "LF1.ActivationTypeAttack",
  aoo: "LF1.ActivationTypeAoO",
  minute: "LF1.ActivationTypeMinute",
  hour: "LF1.ActivationTypeHour",
  special: "LF1.ActivationTypeSpecial",
};

/**
 * This describes plurals for the ways that an ability can be activated when
 * using Unchained rules.
 */
export const abilityActivationTypesPlurals_unchained = {
  passive: "LF1.ActivationTypePassive",
  free: "LF1.ActivationTypeFreePlural",
  reaction: "LF1.ActivationTypeReactionPlural",
  action: "LF1.ActivationTypeActionPlural",
  minute: "LF1.ActivationTypeMinutePlural",
  hour: "LF1.ActivationTypeHourPlural",
  special: "LF1.ActivationTypeSpecial",
};

/**
 * The possible conditions when using Wound Threshold rules
 */
export const woundThresholdConditions = {
  0: "LF1.WoundLevelHealthy",
  1: "LF1.WoundLevelGrazed",
  2: "LF1.WoundLevelWounded",
  3: "LF1.WoundLevelCritical",
};

/**
 * Change targets affected by Wound Thresholds.
 */
export const woundThresholdChangeTargets = ["~attackCore", "cmd", "allSavingThrows", "ac", "~skillMods", "allChecks"];

export const divineFocus = {
  0: "",
  1: "LF1.SpellComponentDivineFocusAlone",
  2: "LF1.SpellComponentDivineFocusMaterial",
  3: "LF1.SpellComponentDivineFocusFocus",
};

/**
 * The measure template types available e.g. for spells
 */
export const measureTemplateTypes = {
  cone: "LF1.MeasureTemplateCone",
  circle: "LF1.MeasureTemplateCircle",
  ray: "LF1.MeasureTemplateRay",
  rect: "LF1.MeasureTemplateRectangle",
};

/* -------------------------------------------- */

/**
 * The possible creature sizes
 */
export const actorSizes = {
  fine: "LF1.ActorSizeFine",
  dim: "LF1.ActorSizeDiminutive",
  tiny: "LF1.ActorSizeTiny",
  sm: "LF1.ActorSizeSmall",
  med: "LF1.ActorSizeMedium",
  lg: "LF1.ActorSizeLarge",
  huge: "LF1.ActorSizeHuge",
  grg: "LF1.ActorSizeGargantuan",
  col: "LF1.ActorSizeColossal",
};

/**
 * The possible creature sizes in their one-letter form
 */
export const sizeChart = {
  fine: "F",
  dim: "D",
  tiny: "T",
  sm: "S",
  med: "M",
  lg: "L",
  huge: "H",
  grg: "G",
  col: "C",
};

/**
 * The size values for Tokens according to the creature's size
 */
export const tokenSizes = {
  fine: { w: 1, h: 1, scale: 0.45 },
  dim: { w: 1, h: 1, scale: 0.6 },
  tiny: { w: 1, h: 1, scale: 0.75 },
  sm: { w: 1, h: 1, scale: 0.9 },
  med: { w: 1, h: 1, scale: 1 },
  lg: { w: 2, h: 2, scale: 1 },
  huge: { w: 3, h: 3, scale: 1 },
  grg: { w: 4, h: 4, scale: 1 },
  col: { w: 6, h: 6, scale: 1 },
};

/**
 * The size modifier applied to creatures not of medium size
 */
export const sizeMods = {
  fine: 8,
  dim: 4,
  tiny: 2,
  sm: 1,
  med: 0,
  lg: -1,
  huge: -2,
  grg: -4,
  col: -8,
};

/**
 * The size modifier applied to creatures not of medium size
 */
export const sizeSpecialMods = {
  fine: -8,
  dim: -4,
  tiny: -2,
  sm: -1,
  med: 0,
  lg: 1,
  huge: 2,
  grg: 4,
  col: 8,
};

/**
 * The size modifier applied to fly checks of creatures not of medium size
 */
export const sizeFlyMods = {
  fine: 8,
  dim: 6,
  tiny: 4,
  sm: 2,
  med: 0,
  lg: -2,
  huge: -4,
  grg: -6,
  col: -8,
};

/**
 * The size modifier applied to stealth checks of creatures not of medium size
 */
export const sizeStealthMods = {
  fine: 16,
  dim: 12,
  tiny: 8,
  sm: 4,
  med: 0,
  lg: -4,
  huge: -8,
  grg: -12,
  col: -16,
};

/**
 * Vehicle properties based on size and type
 */
export const vehicles = {
  size: {
    lg: {
      label: "LF1.ActorSizeLarge",
      space: "LF1.Vehicles.Space.Large",
    },
    huge: {
      label: "LF1.ActorSizeHuge",
      space: "LF1.Vehicles.Space.Huge",
    },
    grg: {
      label: "LF1.ActorSizeGargantuan",
      space: "LF1.Vehicles.Space.Gargantuan",
    },
    col: {
      label: "LF1.ActorSizeColossal",
      space: "LF1.Vehicles.Space.Colossal",
    },
  },
  type: {
    land: "LF1.Vehicles.Types.Land",
    sea: "LF1.Vehicles.Types.Sea",
    air: "LF1.Vehicles.Types.Air",
  },
};

/**
 * The possible options for a creature's maneuverability
 */
export const flyManeuverabilities = {
  clumsy: "LF1.FlyManeuverabilityClumsy",
  poor: "LF1.FlyManeuverabilityPoor",
  average: "LF1.FlyManeuverabilityAverage",
  good: "LF1.FlyManeuverabilityGood",
  perfect: "LF1.FlyManeuverabilityPerfect",
};

/**
 * The bonus values for a creature's maneuverability
 */
export const flyManeuverabilityValues = {
  clumsy: -8,
  poor: -4,
  average: 0,
  good: 4,
  perfect: 8,
};

/**
 * The resulting speed values when a base speed is reduced
 */
export const speedReduction = {
  5: 5,
  15: 10,
  20: 15,
  30: 20,
  35: 25,
  45: 30,
  50: 35,
  60: 40,
  65: 45,
  75: 50,
  80: 55,
  90: 60,
  95: 65,
  105: 70,
  110: 75,
  120: 80,
};

/* -------------------------------------------- */

/**
 * An array of maximum carry capacities, where the index is the ability/strength score.
 */
// prettier-ignore
export const encumbranceLoads =  [
    0,
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    100,
    115,
    130,
    150,
    175,
    200,
    230,
    260,
    300,
    350,
    400,
    460,
    520,
    600,
    700,
    800,
    920,
    1040,
    1200,
    1400,
    1600,
  ];

/**
 * Encumbrance levels for light, medium, and heavy loads.
 *
 * @see {@link ActorPF._computeEncumbrance ActorPF.system.encumbrance.level}
 * @readonly
 * @enum {number}
 */
export const encumbranceLevels = {
  light: 0,
  medium: 1,
  heavy: 2,
};

/**
 * Encumbrance multipliers applied due to a creature's size for bi- and
 * quadrupedal creatures.
 */
export const encumbranceMultipliers = {
  normal: {
    fine: 0.125,
    dim: 0.25,
    tiny: 0.5,
    sm: 0.75,
    med: 1,
    lg: 2,
    huge: 4,
    grg: 8,
    col: 16,
  },
  quadruped: {
    fine: 0.25,
    dim: 0.5,
    tiny: 0.75,
    sm: 1,
    med: 1.5,
    lg: 3,
    huge: 6,
    grg: 12,
    col: 24,
  },
};

/**
 * Damage multipliers from ability score.
 */
export const abilityDamageMultipliers = [
  { value: 0.5, label: "×0.5" },
  { value: 1, label: "×1" },
  { value: 1.5, label: "×1.5" },
  { value: 2, label: "×2" },
  { value: 2.5, label: "×2.5" },
];

/* -------------------------------------------- */

/**
 * The types for Items
 */
export const itemTypes = {
  equipment: "LF1.ItemTypeEquipment",
  weapon: "LF1.ItemTypeWeapon",
  loot: "LF1.ItemTypeLoot",
  consumable: "LF1.ItemTypeConsumable",
  class: "LF1.ItemTypeClass",
  buff: "LF1.ItemTypeBuff",
  spell: "LF1.ItemTypeSpell",
  feat: "LF1.ItemTypeFeat",
  attack: "LF1.ItemTypeAttack",
};

/**
 * Classification types for item action types
 */
export const itemActionTypes = {
  mwak: "LF1.ActionMWAK",
  rwak: "LF1.ActionRWAK",
  msak: "LF1.ActionMSAK",
  rsak: "LF1.ActionRSAK",
  mcman: "LF1.ActionMCMan",
  rcman: "LF1.ActionRCMan",
  spellsave: "LF1.ActionSpellSave",
  save: "LF1.ActionSave",
  heal: "LF1.ActionHeal",
  other: "LF1.ActionOther",
};

/* -------------------------------------------- */

export const itemCapacityTypes = {
  items: "LF1.ItemContainerCapacityItems",
  weight: "LF1.ItemContainerCapacityWeight",
};

/* -------------------------------------------- */

/**
 * Enumerate the lengths of time over which an item can have limited use ability
 */
export const limitedUsePeriods = {
  single: "LF1.LimitedUseSingle",
  unlimited: "LF1.Unlimited",
  day: "LF1.LimitedUseDay",
  week: "LF1.LimitedUseWeek",
  charges: "LF1.LimitedUseCharges",
};

/* -------------------------------------------- */

/**
 * The various equipment types and their subtypes
 */
export const equipmentTypes = {
  armor: {
    _label: "LF1.EquipTypeArmor",
    lightArmor: "LF1.EquipTypeLight",
    mediumArmor: "LF1.EquipTypeMedium",
    heavyArmor: "LF1.EquipTypeHeavy",
  },
  shield: {
    _label: "LF1.EquipTypeShield",
    lightShield: "LF1.EquipTypeLightShield",
    heavyShield: "LF1.EquipTypeHeavyShield",
    towerShield: "LF1.EquipTypeTowerShield",
    other: "LF1.EquipTypeOtherShield",
  },
  wondrous: {
    _label: "LF1.EquipTypeWondrousItem",
  },
  clothing: {
    _label: "LF1.EquipTypeClothing",
  },
  other: {
    _label: "LF1.Other",
  },
};

/**
 * The slots equipment can occupy, sorted by category
 */
export const equipmentSlots = {
  armor: {
    armor: "LF1.EquipSlotArmor",
  },
  shield: {
    shield: "LF1.EquipSlotShield",
  },
  wondrous: {
    slotless: "LF1.EquipSlotSlotless",
    head: "LF1.EquipSlotHead",
    headband: "LF1.EquipSlotHeadband",
    eyes: "LF1.EquipSlotEyes",
    shoulders: "LF1.EquipSlotShoulders",
    neck: "LF1.EquipSlotNeck",
    chest: "LF1.EquipSlotChest",
    body: "LF1.EquipSlotBody",
    belt: "LF1.EquipSlotBelt",
    wrists: "LF1.EquipSlotWrists",
    hands: "LF1.EquipSlotHands",
    ring: "LF1.EquipSlotRing",
    feet: "LF1.EquipSlotFeet",
  },
  clothing: {
    clothing: "LF1.EquipTypeClothing",
  },
  other: {
    other: "LF1.Other",
  },
};

/**
 * The subtypes for loot items
 */
export const lootTypes = {
  gear: "LF1.LootTypeGear",
  ammo: "LF1.LootTypeAmmo",
  tradeGoods: "LF1.LootTypeTradeGoods",
  misc: "LF1.Misc",
};

/**
 * The subtypes for ammo type loot items
 */
export const ammoTypes = {
  arrow: "LF1.AmmoTypeArrow",
  bolt: "LF1.AmmoTypeBolt",
  repeatingBolt: "LF1.AmmoTypeRepeatingBolt",
  slingBullet: "LF1.AmmoTypeBulletSling",
  gunBullet: "LF1.AmmoTypeBulletGun",
  dragoonBullet: "LF1.AmmoTypeBulletDragoon",
  dart: "LF1.AmmoTypeDart",
};

/* -------------------------------------------- */

/**
 * Enumerate the valid consumable types which are recognized by the system
 */
export const consumableTypes = {
  potion: "LF1.ConsumableTypePotion",
  poison: "LF1.ConsumableTypePoison",
  drug: "LF1.ConsumableTypeDrug",
  scroll: "LF1.ConsumableTypeScroll",
  wand: "LF1.ConsumableTypeWand",
  rod: "LF1.ConsumableTypeRod",
  staff: "LF1.ConsumableTypeStaff",
  misc: "LF1.Misc",
};

export const attackTypes = {
  weapon: "LF1.AttackTypeWeapon",
  natural: "LF1.AttackTypeNatural",
  ability: "LF1.AttackTypeAbility",
  racialAbility: "LF1.AttackTypeRacial",
  item: "LF1.Item",
  misc: "LF1.Misc",
};

export const featTypes = {
  feat: "LF1.FeatTypeFeat",
  classFeat: "LF1.FeatTypeClassFeat",
  trait: "LF1.FeatTypeTraits",
  racial: "LF1.FeatTypeRacial",
  misc: "LF1.Misc",
  template: "LF1.FeatTypeTemplate",
};

export const featTypesPlurals = {
  feat: "LF1.FeatPlural",
  classFeat: "LF1.ClassFeaturePlural",
  trait: "LF1.TraitPlural",
  racial: "LF1.RacialTraitPlural",
  template: "LF1.TemplatePlural",
};

export const traitTypes = {
  combat: "LF1.Trait.Combat",
  magic: "LF1.Trait.Magic",
  faith: "LF1.Trait.Faith",
  social: "LF1.Trait.Social",
  campaign: "LF1.Trait.Campaign",
  cosmic: "LF1.Trait.Cosmic",
  equipment: "LF1.Trait.Equipment",
  exemplar: "LF1.Trait.Exemplar",
  faction: "LF1.Trait.Faction",
  family: "LF1.Trait.Family",
  mount: "LF1.Trait.Mount",
  race: "LF1.Trait.Race",
  region: "LF1.Trait.Region",
  religion: "LF1.Trait.Religion",
  drawback: "LF1.Trait.Drawback",
};

/**
 * Ability types, each with their short and their long form
 */
export const abilityTypes = {
  ex: {
    short: "LF1.AbilityTypeShortExtraordinary",
    long: "LF1.AbilityTypeExtraordinary",
  },
  su: {
    short: "LF1.AbilityTypeShortSupernatural",
    long: "LF1.AbilityTypeSupernatural",
  },
  sp: {
    short: "LF1.AbilityTypeShortSpell-Like",
    long: "LF1.AbilityTypeSpell-Like",
  },
};

/* -------------------------------------------- */

/**
 * The valid currency denominations supported by the game system
 */
export const currencies = {
  pp: "LF1.CurrencyPP",
  gp: "LF1.CurrencyGP",
  sp: "LF1.CurrencySP",
  cp: "LF1.CurrencyCP",
};

/**
 * Resultant armor types for an actor's worn armor as per their roll data
 *
 * @see {@link ActorPF.getRollData ActorRollData.armor.type}
 * @readonly
 * @enum {number}
 */
export const armorTypes = {
  none: 0,
  light: 1,
  medium: 2,
  heavy: 3,
};

/**
 * Resultant shield types for an actor's worn shield
 *
 * @see {@link ActorPF.getRollData ActorRollData.shield.type}
 * @readonly
 * @enum {number}
 */
export const shieldTypes = {
  none: 0,
  other: 1, // buckler
  light: 2,
  heavy: 3,
  tower: 4,
};

/**
 * The types of bonus modifiers
 */
export const bonusModifiers = {
  untyped: "LF1.BonusModifierUntyped",
  untypedPerm: "LF1.BonusModifierUntypedPerm",
  base: "LF1.BonusModifierBase",
  enh: "LF1.BonusModifierEnhancement",
  dodge: "LF1.BonusModifierDodge",
  haste: "LF1.BonusModifierHaste",
  inherent: "LF1.BonusModifierInherent",
  deflection: "LF1.BonusModifierDeflection",
  morale: "LF1.BonusModifierMorale",
  luck: "LF1.BonusModifierLuck",
  sacred: "LF1.BonusModifierSacred",
  insight: "LF1.BonusModifierInsight",
  resist: "LF1.BonusModifierResistance",
  profane: "LF1.BonusModifierProfane",
  trait: "LF1.BonusModifierTrait",
  racial: "LF1.BonusModifierRacial",
  size: "LF1.BonusModifierSize",
  competence: "LF1.BonusModifierCompetence",
  circumstance: "LF1.BonusModifierCircumstance",
  alchemical: "LF1.BonusModifierAlchemical",
  penalty: "LF1.BonusModifierPenalty",
};

/**
 * An array of stacking bonus modifiers by their keys for {@link bonusModifiers}
 */
export const stackingBonusModifiers = ["untyped", "untypedPerm", "dodge", "racial", "penalty"];

/* -------------------------------------------- */

/* -------------------------------------------- */

/**
 * Valid options for the range of abilities and spells
 */
export const distanceUnits = {
  none: "LF1.None",
  personal: "LF1.DistPersonal",
  touch: "LF1.DistTouch",
  melee: "LF1.DistMelee",
  reach: "LF1.DistReach",
  close: "LF1.DistClose",
  medium: "LF1.DistMedium",
  long: "LF1.DistLong",
  ft: "LF1.DistFt",
  mi: "LF1.DistMi",
  spec: "LF1.Special",
  seeText: "LF1.SeeText",
  unlimited: "LF1.Unlimited",
};

export const measureUnits = {
  ft: "LF1.DistFt",
  mi: "LF1.DistMi",
  m: "LF1.DistM",
  km: "LF1.DistKM",
};

export const measureUnitsShort = {
  ft: "LF1.DistFtShort",
  mi: "LF1.DistMiShort",
  m: "LF1.DistMShort",
  km: "LF1.DistKMShort",
};

export const actorStatures = {
  tall: "LF1.StatureTall",
  long: "LF1.StatureLong",
};

/* -------------------------------------------- */

/**
 * This Object defines the types of single or area targets which can be applied in the game system.
 */
export const targetTypes = {
  none: "LF1.None",
  self: "LF1.TargetSelf",
  creature: "LF1.TargetCreature",
  ally: "LF1.TargetAlly",
  enemy: "LF1.TargetEnemy",
  object: "LF1.TargetObject",
  space: "LF1.TargetSpace",
  radius: "LF1.TargetRadius",
  sphere: "LF1.TargetSphere",
  cylinder: "LF1.TargetCylinder",
  cone: "LF1.TargetCone",
  square: "LF1.TargetSquare",
  cube: "LF1.TargetCube",
  line: "LF1.TargetLine",
  wall: "LF1.TargetWall",
};

/* -------------------------------------------- */

/**
 * This Object defines the various lengths of time which can occur in LF1
 */
export const timePeriods = {
  inst: "LF1.TimeInst",
  turn: "LF1.TimeTurn",
  round: "LF1.TimeRound",
  minute: "LF1.TimeMinute",
  hour: "LF1.TimeHour",
  day: "LF1.TimeDay",
  month: "LF1.TimeMonth",
  year: "LF1.TimeYear",
  perm: "LF1.TimePerm",
  seeText: "LF1.SeeText",
  spec: "LF1.Special",
};

export const timePeriodsShort = {
  turn: "LF1.TimeTurnShort",
  round: "LF1.TimeRoundShort",
  minute: "LF1.TimeMinuteShort",
  hour: "LF1.TimeHourShort",
};

/* -------------------------------------------- */

/**
 * This Object determines spells gained and cast per level
 */
export const casterProgression = {
  castsPerDay: {
    prepared: {
      low: [
        [Number.POSITIVE_INFINITY],
        [Number.POSITIVE_INFINITY],
        [Number.POSITIVE_INFINITY],
        [Number.POSITIVE_INFINITY, 0],
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 1, 0],
        [Number.POSITIVE_INFINITY, 1, 1],
        [Number.POSITIVE_INFINITY, 2, 1],
        [Number.POSITIVE_INFINITY, 2, 1, 0],
        [Number.POSITIVE_INFINITY, 2, 1, 1],
        [Number.POSITIVE_INFINITY, 2, 2, 1],
        [Number.POSITIVE_INFINITY, 3, 2, 1, 0],
        [Number.POSITIVE_INFINITY, 3, 2, 1, 1],
        [Number.POSITIVE_INFINITY, 3, 2, 2, 1],
        [Number.POSITIVE_INFINITY, 3, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 3, 2, 2],
        [Number.POSITIVE_INFINITY, 4, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 3, 3],
      ],
      med: [
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 2],
        [Number.POSITIVE_INFINITY, 3],
        [Number.POSITIVE_INFINITY, 3, 1],
        [Number.POSITIVE_INFINITY, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 5, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 5, 4],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 5, 5],
      ],
      high: [
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 2],
        [Number.POSITIVE_INFINITY, 2, 1],
        [Number.POSITIVE_INFINITY, 3, 2],
        [Number.POSITIVE_INFINITY, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 4, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 3, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 4, 4],
      ],
    },
    spontaneous: {
      low: [
        [Number.POSITIVE_INFINITY],
        [Number.POSITIVE_INFINITY],
        [Number.POSITIVE_INFINITY],
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 1, 1],
        [Number.POSITIVE_INFINITY, 1, 1],
        [Number.POSITIVE_INFINITY, 2, 1],
        [Number.POSITIVE_INFINITY, 2, 1, 1],
        [Number.POSITIVE_INFINITY, 2, 1, 1],
        [Number.POSITIVE_INFINITY, 2, 2, 1],
        [Number.POSITIVE_INFINITY, 3, 2, 1, 1],
        [Number.POSITIVE_INFINITY, 3, 2, 1, 1],
        [Number.POSITIVE_INFINITY, 3, 2, 2, 1],
        [Number.POSITIVE_INFINITY, 3, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 3, 2, 1],
        [Number.POSITIVE_INFINITY, 4, 3, 2, 2],
        [Number.POSITIVE_INFINITY, 4, 3, 3, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 3, 2],
      ],
      med: [
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 2],
        [Number.POSITIVE_INFINITY, 3],
        [Number.POSITIVE_INFINITY, 3, 1],
        [Number.POSITIVE_INFINITY, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 5, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 5, 4],
        [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 5, 5],
      ],
      high: [
        [Number.POSITIVE_INFINITY, 3],
        [Number.POSITIVE_INFINITY, 4],
        [Number.POSITIVE_INFINITY, 5],
        [Number.POSITIVE_INFINITY, 6, 3],
        [Number.POSITIVE_INFINITY, 6, 4],
        [Number.POSITIVE_INFINITY, 6, 5, 3],
        [Number.POSITIVE_INFINITY, 6, 6, 4],
        [Number.POSITIVE_INFINITY, 6, 6, 5, 3],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 4],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 5, 3],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 4],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 5, 3],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 4],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 5, 3],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 4],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 5, 3],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 6, 4],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 6, 5, 3],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 6, 6, 4],
        [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 6, 6, 6],
      ],
    },
    hybrid: {
      high: [
        [Number.POSITIVE_INFINITY, 2],
        [Number.POSITIVE_INFINITY, 3],
        [Number.POSITIVE_INFINITY, 4],
        [Number.POSITIVE_INFINITY, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 4, 4],
      ],
    },
    prestige: {
      low: [
        [Number.POSITIVE_INFINITY, 1],
        [Number.POSITIVE_INFINITY, 2],
        [Number.POSITIVE_INFINITY, 3],
        [Number.POSITIVE_INFINITY, 3, 1],
        [Number.POSITIVE_INFINITY, 4, 2],
        [Number.POSITIVE_INFINITY, 4, 3],
        [Number.POSITIVE_INFINITY, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 4, 4, 2],
        [Number.POSITIVE_INFINITY, 5, 4, 3],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
      ],
    },
  },
  spellsPreparedPerDay: {
    prepared: {
      low: [
        [null],
        [null],
        [null],
        [null, 0],
        [null, 1],
        [null, 1],
        [null, 1, 0],
        [null, 1, 1],
        [null, 2, 1],
        [null, 2, 1, 0],
        [null, 2, 1, 1],
        [null, 2, 2, 1],
        [null, 3, 2, 1, 0],
        [null, 3, 2, 1, 1],
        [null, 3, 2, 2, 1],
        [null, 3, 3, 2, 1],
        [null, 4, 3, 2, 1],
        [null, 4, 3, 2, 2],
        [null, 4, 3, 3, 2],
        [null, 4, 4, 3, 3],
      ],
      med: [
        [3, 1],
        [4, 2],
        [4, 3],
        [4, 3, 1],
        [4, 4, 2],
        [5, 4, 3],
        [5, 4, 3, 1],
        [5, 4, 4, 2],
        [5, 5, 4, 3],
        [5, 5, 4, 3, 1],
        [5, 5, 4, 4, 2],
        [5, 5, 5, 4, 3],
        [5, 5, 5, 4, 3, 1],
        [5, 5, 5, 4, 4, 2],
        [5, 5, 5, 5, 4, 3],
        [5, 5, 5, 5, 4, 3, 1],
        [5, 5, 5, 5, 4, 4, 2],
        [5, 5, 5, 5, 5, 4, 3],
        [5, 5, 5, 5, 5, 5, 4],
        [5, 5, 5, 5, 5, 5, 5],
      ],
      high: [
        [3, 1],
        [4, 2],
        [4, 2, 1],
        [4, 3, 2],
        [4, 3, 2, 1],
        [4, 3, 3, 2],
        [4, 4, 3, 2, 1],
        [4, 4, 3, 3, 2],
        [4, 4, 4, 3, 2, 1],
        [4, 4, 4, 3, 3, 2],
        [4, 4, 4, 4, 3, 2, 1],
        [4, 4, 4, 4, 3, 3, 2],
        [4, 4, 4, 4, 4, 3, 2, 1],
        [4, 4, 4, 4, 4, 3, 3, 2],
        [4, 4, 4, 4, 4, 4, 3, 2, 1],
        [4, 4, 4, 4, 4, 4, 3, 3, 2],
        [4, 4, 4, 4, 4, 4, 4, 3, 2, 1],
        [4, 4, 4, 4, 4, 4, 4, 3, 3, 2],
        [4, 4, 4, 4, 4, 4, 4, 4, 3, 3],
        [4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
      ],
    },
    spontaneous: {
      low: [
        [2],
        [3],
        [4],
        [4, 2],
        [5, 3],
        [5, 4],
        [6, 4, 2],
        [6, 4, 3],
        [6, 5, 4],
        [6, 5, 4, 2],
        [6, 5, 4, 3],
        [6, 6, 5, 4],
        [6, 6, 5, 4, 2],
        [6, 6, 5, 4, 3],
        [6, 6, 6, 5, 4],
        [6, 6, 6, 5, 4],
        [6, 6, 6, 5, 4],
        [6, 6, 6, 6, 5],
        [6, 6, 6, 6, 5],
        [6, 6, 6, 6, 5],
      ],
      med: [
        [4, 2],
        [5, 3],
        [6, 4],
        [6, 4, 2],
        [6, 4, 3],
        [6, 4, 4],
        [6, 5, 4, 2],
        [6, 5, 4, 3],
        [6, 5, 4, 4],
        [6, 5, 5, 4, 2],
        [6, 6, 5, 4, 3],
        [6, 6, 5, 4, 4],
        [6, 6, 5, 5, 4, 2],
        [6, 6, 6, 5, 4, 3],
        [6, 6, 6, 5, 4, 4],
        [6, 6, 6, 5, 5, 4, 2],
        [6, 6, 6, 6, 5, 4, 3],
        [6, 6, 6, 6, 5, 4, 4],
        [6, 6, 6, 6, 5, 5, 4],
        [6, 6, 6, 6, 6, 5, 5],
      ],
      high: [
        [4, 2],
        [5, 2],
        [5, 3],
        [6, 3, 1],
        [6, 4, 2],
        [7, 4, 2, 1],
        [7, 5, 3, 2],
        [8, 5, 3, 2, 1],
        [8, 5, 4, 3, 2],
        [9, 5, 4, 3, 2, 1],
        [9, 5, 5, 4, 3, 2],
        [9, 5, 5, 4, 3, 2, 1],
        [9, 5, 5, 4, 4, 3, 2],
        [9, 5, 5, 4, 4, 3, 2, 1],
        [9, 5, 5, 4, 4, 4, 3, 2],
        [9, 5, 5, 4, 4, 4, 3, 2, 1],
        [9, 5, 5, 4, 4, 4, 3, 3, 2],
        [9, 5, 5, 4, 4, 4, 3, 3, 2, 1],
        [9, 5, 5, 4, 4, 4, 3, 3, 3, 2],
        [9, 5, 5, 4, 4, 4, 3, 3, 3, 3],
      ],
    },
    hybrid: {
      high: [
        [4, 2],
        [5, 2],
        [5, 3],
        [6, 3, 1],
        [6, 4, 2],
        [7, 4, 2, 1],
        [7, 5, 3, 2],
        [8, 5, 3, 2, 1],
        [8, 5, 4, 3, 2],
        [9, 5, 4, 3, 2, 1],
        [9, 5, 5, 4, 3, 2],
        [9, 5, 5, 4, 3, 2, 1],
        [9, 5, 5, 4, 4, 3, 2],
        [9, 5, 5, 4, 4, 3, 2, 1],
        [9, 5, 5, 4, 4, 4, 3, 2],
        [9, 5, 5, 4, 4, 4, 3, 2, 1],
        [9, 5, 5, 4, 4, 4, 3, 3, 2],
        [9, 5, 5, 4, 4, 4, 3, 3, 2, 1],
        [9, 5, 5, 4, 4, 4, 3, 3, 3, 2],
        [9, 5, 5, 4, 4, 4, 3, 3, 3, 3],
      ],
    },
    prestige: {
      low: [
        [null, 2],
        [null, 3],
        [null, 4],
        [null, 4, 2],
        [null, 4, 3],
        [null, 4, 4],
        [null, 5, 4, 2],
        [null, 5, 4, 3],
        [null, 5, 4, 4],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
        [null, 5, 5, 4, 2],
      ],
    },
  },
};

/* -------------------------------------------- */

// Healing Types
/**
 * Types of healing
 */
export const healingTypes = {
  healing: "LF1.Healing",
  temphp: "LF1.HealingTemp",
};

/* -------------------------------------------- */

/**
 * Character senses options
 */
export const senses = {
  bs: "LF1.SenseBS",
  bse: "LF1.SenseBSense",
  dv: "LF1.SenseDV",
  ts: "LF1.SenseTS",
  tr: "LF1.SenseTR",
  ll: "LF1.SenseLL",
  si: "LF1.SenseSI",
  sid: "LF1.SenseSID",
  sc: "LF1.SenseSC",
};

/* -------------------------------------------- */

/**
 * The set of skill which can be trained in LF1
 */
export const skills = {
  acr: "LF1.SkillAcr",
  apr: "LF1.SkillApr",
  art: "LF1.SkillArt",
  blf: "LF1.SkillBlf",
  clm: "LF1.SkillClm",
  crf: "LF1.SkillCrf",
  dip: "LF1.SkillDip",
  dev: "LF1.SkillDev",
  dis: "LF1.SkillDis",
  esc: "LF1.SkillEsc",
  fly: "LF1.SkillFly",
  han: "LF1.SkillHan",
  hea: "LF1.SkillHea",
  int: "LF1.SkillInt",
  kar: "LF1.SkillKAr",
  kdu: "LF1.SkillKDu",
  ken: "LF1.SkillKEn",
  kge: "LF1.SkillKGe",
  khi: "LF1.SkillKHi",
  klo: "LF1.SkillKLo",
  kna: "LF1.SkillKNa",
  kno: "LF1.SkillKNo",
  kpl: "LF1.SkillKPl",
  kre: "LF1.SkillKRe",
  lin: "LF1.SkillLin",
  lor: "LF1.SkillLor",
  per: "LF1.SkillPer",
  prf: "LF1.SkillPrf",
  pro: "LF1.SkillPro",
  rid: "LF1.SkillRid",
  sen: "LF1.SkillSen",
  slt: "LF1.SkillSlt",
  spl: "LF1.SkillSpl",
  ste: "LF1.SkillSte",
  sur: "LF1.SkillSur",
  swm: "LF1.SkillSwm",
  umd: "LF1.SkillUMD",
};

/**
 * Compendium journal entries containing details about {@link skills}
 */
export const skillCompendiumEntries = {
  acr: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.gGfJO90ZuRT4sZ9X",
  apr: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.QGxoGsSIAOoe5dTW",
  art: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.x175kVqUfLGPt8tC",
  blf: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.kRK5XwVBvcMi35w2",
  clm: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.ZAwjVOTwsBpZRgw4",
  crf: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.3E8pxbjI8MD3JbfQ",
  dip: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.uB9Fy36RUjibxqvt",
  dev: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.cSdUATLHBFfw3v4s",
  dis: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.xg25z3GIpS590NDW",
  esc: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.DTNlXXg77s3178WJ",
  fly: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.iH69GIwm8BjecrPN",
  han: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.IrVgSeMcAM8rAh2B",
  hea: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.nHbYSOoe1SzqEO9w",
  int: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.KNn8uxHu23phKC0y",
  kar: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  kdu: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  ken: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  kge: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  khi: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  klo: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  kna: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  kno: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  kpl: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  kre: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.BWeqgXSZvUQl68vt",
  lin: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.SqLm3Deag2FpP8ty",
  lor: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.x175kVqUfLGPt8tC",
  per: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.2h6hz5AkTDxKPFxr",
  prf: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.emzBKDFNkNnC7N8u",
  pro: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.224EaK0K72NhCeRi",
  rid: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.xQbTtefpEfEaOYo7",
  sen: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.ka0VQGItdrWw3paO",
  slt: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.joza8kAIiImrPft7",
  spl: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.VRD7jxiIAxKPt6EF",
  ste: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.wRWHk7tCUHR99PzD",
  sur: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.pLdYavy4nssLEoGV",
  swm: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.xhmDhOXuBbfVcD0Q",
  umd: "Compendium.lf1.lf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.A8j7nF6qHwuGEC2E",
};

/**
 * An array of {@link skills} that can have arbitrary subskills
 */
export const arbitrarySkills = ["art", "crf", "lor", "prf", "pro"];

/**
 * An array of {@link skills} that are considered background skills.
 */
export const backgroundSkills = [
  "apr",
  "art",
  "crf",
  "han",
  "ken",
  "kge",
  "khi",
  "kno",
  "lin",
  "lor",
  "prf",
  "pro",
  "slt",
];

/**
 * Array of skills that are only shown with background skills optional rule enabled.
 */
export const backgroundOnlySkills = ["lor", "art"];

/*
 * Number of background skills per level gained from valid classes.
 */
export const backgroundSkillsPerLevel = 2;

/**
 * Valid class types to grant background skills.
 */
export const backgroundSkillClasses = ["base", "prestige"];

/**
 * Bonus modifier granted to class skills.
 */
export const classSkillBonus = 3;

/* -------------------------------------------- */

/**
 * Valid options for how a spell is prepared
 */
export const spellPreparationModes = {
  atwill: "LF1.SpellPrepAtWill",
  prepared: "LF1.SpellPrepPrepared",
  spontaneous: "LF1.SpellPrepSpontaneous",
};

export const classCasterType = {
  sorcerer: "high",
  wizard: "high",
  cleric: "high",
  oracle: "high",
  druid: "high",
  psychic: "high",
  shaman: "high",
  witch: "high",
  alchemist: "med",
  bard: "med",
  hunter: "med",
  inquisitor: "med",
  investigator: "med",
  magus: "med",
  mesmerist: "med",
  occultist: "med",
  skald: "med",
  spiritualist: "med",
  summoner: "med",
  "unchained Summoner": "med",
  antipaladin: "low",
  bloodrager: "low",
  medium: "low",
  paladin: "low",
  ranger: "low",
};

export const spellcasting = {
  type: {
    spontaneous: "LF1.SpellPrepSpontaneous",
    prepared: "LF1.SpellPrepPrepared",
    hybrid: "LF1.Arcanist",
  },
  spells: {
    arcane: "LF1.Spellcasting.Type.Arcane",
    divine: "LF1.Spellcasting.Type.Divine",
    psychic: "LF1.Spellcasting.Type.Psychic",
    alchemy: "LF1.Spellcasting.Type.Alchemy",
  },
};

export const magicAuraByLevel = {
  spell: [
    { power: "faint", level: 1 },
    { power: "moderate", level: 4 },
    { power: "strong", level: 7 },
    { power: "overwhelming", level: 10 },
  ],
  item: [
    { power: "faint", level: 1 },
    { power: "moderate", level: 6 },
    { power: "strong", level: 12 },
    { power: "overwhelming", level: 21 },
  ],
};

export const auraStrengths = {
  1: "LF1.AuraStrength_Faint",
  2: "LF1.AuraStrength_Moderate",
  3: "LF1.AuraStrength_Strong",
  4: "LF1.AuraStrength_Overwhelming",
};

/* -------------------------------------------- */

/* -------------------------------------------- */

// Weapon Types
export const weaponTypes = {
  simple: {
    _label: "LF1.WeaponTypeSimple",
    light: "LF1.WeaponPropLight",
    "1h": "LF1.WeaponPropOneHanded",
    "2h": "LF1.WeaponPropTwoHanded",
    ranged: "LF1.WeaponSubtypeRanged",
  },
  martial: {
    _label: "LF1.WeaponTypeMartial",
    light: "LF1.WeaponPropLight",
    "1h": "LF1.WeaponPropOneHanded",
    "2h": "LF1.WeaponPropTwoHanded",
    ranged: "LF1.WeaponSubtypeRanged",
  },
  exotic: {
    _label: "LF1.WeaponTypeExotic",
    light: "LF1.WeaponPropLight",
    "1h": "LF1.WeaponPropOneHanded",
    "2h": "LF1.WeaponPropTwoHanded",
    ranged: "LF1.WeaponSubtypeRanged",
  },
  misc: {
    _label: "LF1.Misc",
    splash: "LF1.WeaponTypeSplash",
    other: "LF1.Other",
  },
};

// Weapon hold types
export const weaponHoldTypes = {
  normal: "LF1.WeaponHoldTypeNormal",
  "2h": "LF1.WeaponHoldTypeTwoHanded",
  oh: "LF1.WeaponHoldTypeOffhand",
};

/**
 * Weapon groups that a weapon can belong to
 */
export const weaponGroups = {
  axes: "LF1.WeaponGroupAxes",
  bladesHeavy: "LF1.WeaponGroupBladesHeavy",
  bladesLight: "LF1.WeaponGroupBladesLight",
  bows: "LF1.WeaponGroupBows",
  close: "LF1.WeaponGroupClose",
  crossbows: "LF1.WeaponGroupCrossbows",
  double: "LF1.WeaponGroupDouble",
  firearms: "LF1.WeaponGroupFirearms",
  flails: "LF1.WeaponGroupFlails",
  hammers: "LF1.WeaponGroupHammers",
  monk: "LF1.WeaponGroupMonk",
  natural: "LF1.WeaponGroupNatural",
  polearms: "LF1.WeaponGroupPolearms",
  siegeEngines: "LF1.WeaponGroupSiegeEngines",
  spears: "LF1.WeaponGroupSpears",
  thrown: "LF1.WeaponGroupThrown",
  tribal: "LF1.WeaponGroupTribal",
};

/* -------------------------------------------- */

/**
 * Define the set of weapon property flags which can exist on a weapon
 */
export const weaponProperties = {
  ato: "LF1.WeaponPropAutomatic",
  blc: "LF1.WeaponPropBlocking",
  brc: "LF1.WeaponPropBrace",
  dea: "LF1.WeaponPropDeadly",
  dst: "LF1.WeaponPropDistracting",
  dbl: "LF1.WeaponPropDouble",
  dis: "LF1.WeaponPropDisarm",
  fin: "LF1.WeaponPropFinesse",
  frg: "LF1.WeaponPropFragile",
  grp: "LF1.WeaponPropGrapple",
  imp: "LF1.WeaponPropImprovised",
  mnk: "LF1.WeaponPropMonk",
  nnl: "LF1.WeaponPropNonLethal",
  prf: "LF1.WeaponPropPerformance",
  rch: "LF1.WeaponPropReach",
  sct: "LF1.WeaponPropScatter",
  snd: "LF1.WeaponPropSunder",
  spc: "LF1.WeaponPropSpecial",
  thr: "LF1.WeaponPropThrown",
  trp: "LF1.WeaponPropTrip",
};

/**
 * The components required for casting a spell
 */
export const spellComponents = {
  V: "LF1.SpellComponentVerbal",
  S: "LF1.SpellComponentSomatic",
  T: "LF1.SpellComponentThought",
  E: "LF1.SpellComponentEmotion",
  M: "LF1.SpellComponentMaterial",
  F: "LF1.SpellComponentFocus",
  DF: "LF1.SpellComponentDivineFocus",
};

/**
 * Spell schools
 */
export const spellSchools = {
  abj: "LF1.SpellSchoolAbjuration",
  con: "LF1.SpellSchoolConjuration",
  div: "LF1.SpellSchoolDivination",
  enc: "LF1.SpellSchoolEnchantment",
  evo: "LF1.SpellSchoolEvocation",
  ill: "LF1.SpellSchoolIllusion",
  nec: "LF1.SpellSchoolNecromancy",
  trs: "LF1.SpellSchoolTransmutation",
  uni: "LF1.SpellSchoolUniversal",
  misc: "LF1.Misc",
};

/**
 * Spell levels
 */
export const spellLevels = {
  0: "LF1.SpellLevel0",
  1: "LF1.SpellLevel1",
  2: "LF1.SpellLevel2",
  3: "LF1.SpellLevel3",
  4: "LF1.SpellLevel4",
  5: "LF1.SpellLevel5",
  6: "LF1.SpellLevel6",
  7: "LF1.SpellLevel7",
  8: "LF1.SpellLevel8",
  9: "LF1.SpellLevel9",
};

/* -------------------------------------------- */

/**
 * Weapon proficiency levels
 * Each level provides a proficiency multiplier
 */
export const proficiencyLevels = {
  "-4": "Not Proficient",
  0: "Proficient",
};

/* -------------------------------------------- */

export const conditionTypes = {
  bleed: "LF1.CondTypeBleed",
  blind: "LF1.CondTypeBlind",
  confuse: "LF1.CondTypeConfuse",
  daze: "LF1.CondTypeDaze",
  dazzle: "LF1.CondTypeDazzle",
  deaf: "LF1.CondTypeDeaf",
  deathEffects: "LF1.CondTypeDeathEffects",
  disease: "LF1.CondTypeDisease",
  energyDrain: "LF1.CondTypeEnergyDrain",
  fatigue: "LF1.CondTypeFatigue",
  fear: "LF1.CondTypeFear",
  mindAffecting: "LF1.CondTypeMindAffecting",
  poison: "LF1.CondTypePoison",
  sicken: "LF1.CondTypeSicken",
  paralyze: "LF1.CondTypeParalyze",
  petrify: "LF1.CondTypePetrify",
  stun: "LF1.CondTypeStun",
  sleep: "LF1.CondTypeSleep",
};

export const conditions = {
  bleed: "LF1.CondBleed",
  lf1_blind: "LF1.CondBlind",
  confused: "LF1.CondConfused",
  dazzled: "LF1.CondDazzled",
  lf1_deaf: "LF1.CondDeaf",
  entangled: "LF1.CondEntangled",
  fatigued: "LF1.CondFatigued",
  exhausted: "LF1.CondExhausted",
  grappled: "LF1.CondGrappled",
  helpless: "LF1.CondHelpless",
  incorporeal: "LF1.CondIncorporeal",
  invisible: "LF1.CondInvisible",
  paralyzed: "LF1.CondParalyzed",
  pinned: "LF1.CondPinned",
  lf1_prone: "LF1.CondProne",
  staggered: "LF1.CondStaggered",
  stunned: "LF1.CondStunned",
  shaken: "LF1.CondShaken",
  frightened: "LF1.CondFrightened",
  panicked: "LF1.CondPanicked",
  sickened: "LF1.CondSickened",
  nauseated: "LF1.CondNauseated",
  dazed: "LF1.CondDazed",
  lf1_sleep: "LF1.CondSleep",
  cowering: "LF1.CondCowering",
  squeezing: "LF1.CondSqueezing",
};

/**
 * Conditions that override each other.
 */
export const conditionTracks = {
  fear: ["shaken", "frightened", "panicked"],
  lethargy: ["fatigued", "exhausted"],
};

export const conditionMechanics = {
  lf1_blind: {
    changes: [
      {
        formula: -2,
        subTarget: "ac",
        modifier: "penalty",
      },
    ],
    flags: ["loseDexToAC"],
  },
  dazzled: {
    changes: [
      {
        formula: -1,
        subTarget: "attack",
        modifier: "penalty",
      },
    ],
  },
  lf1_deaf: {
    changes: [
      {
        formula: -4,
        subTarget: "init",
        modifier: "penalty",
      },
    ],
  },
  entangled: {
    changes: [
      {
        formula: -4,
        subTarget: "dex",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "attack",
        modifier: "penalty",
      },
    ],
  },
  grappled: {
    changes: [
      {
        formula: -4,
        subTarget: "dex",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "attack",
        modifier: "penalty",
      },
    ],
  },
  helpless: {
    changes: [
      {
        formula: 0,
        subTarget: "dex",
        modifier: "untypedPerm",
        operator: "set",
        priority: 1001,
        continuous: true,
      },
    ],
  },
  lf1_sleep: {
    changes: [
      {
        formula: 0,
        subTarget: "dex",
        modifier: "untypedPerm",
        operator: "set",
        priority: 1001,
        continuous: true,
      },
    ],
  },
  paralyzed: {
    changes: [
      {
        formula: 0,
        subTarget: "dex",
        modifier: "untypedPerm",
        operator: "set",
        priority: 1001,
        continuous: true,
      },
      {
        formula: 0,
        subTarget: "str",
        modifier: "untypedPerm",
        operator: "set",
        priority: 1001,
        continuous: true,
      },
    ],
  },
  lf1_prone: {
    changes: [
      {
        formula: -4,
        subTarget: "mattack",
        modifier: "penalty",
      },
    ],
  },
  pinned: {
    changes: [
      {
        formula: "min(0, @abilities.dex.mod)",
        subTarget: "dexMod",
        modifier: "untyped",
        operator: "set",
        priority: 1001,
        continuous: true,
      },
      {
        formula: -4,
        subTarget: "ac",
        modifier: "penalty",
      },
      {
        formula: -4,
        subTarget: "cmd",
        modifier: "penalty",
      },
    ],
    flags: ["loseDexToAC"],
  },
  cowering: {
    changes: [
      {
        formula: -2,
        subTarget: "ac",
        modifier: "penalty",
      },
    ],
  },
  shaken: {
    changes: [
      {
        formula: -2,
        subTarget: "attack",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "allSavingThrows",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "skills",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "allChecks",
        modifier: "penalty",
      },
    ],
  },
  frightened: {
    changes: [
      {
        formula: -2,
        subTarget: "attack",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "allSavingThrows",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "skills",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "allChecks",
        modifier: "penalty",
      },
    ],
  },
  panicked: {
    changes: [
      {
        formula: -2,
        subTarget: "attack",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "allSavingThrows",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "skills",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "allChecks",
        modifier: "penalty",
      },
    ],
  },
  sickened: {
    changes: [
      {
        formula: -2,
        subTarget: "attack",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "wdamage",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "allSavingThrows",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "skills",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "allChecks",
        modifier: "penalty",
      },
    ],
  },
  nauseated: {
    // Prevents actions, does not cause modifiers
  },
  stunned: {
    changes: [
      {
        formula: -2,
        subTarget: "ac",
        modifier: "penalty",
      },
    ],
  },
  exhausted: {
    changes: [
      {
        formula: -6,
        subTarget: "str",
        modifier: "penalty",
      },
      {
        formula: -6,
        subTarget: "dex",
        modifier: "penalty",
      },
    ],
  },
  fatigued: {
    changes: [
      {
        formula: -2,
        subTarget: "str",
        modifier: "penalty",
      },
      {
        formula: -2,
        subTarget: "dex",
        modifier: "penalty",
      },
    ],
  },
  squeezing: {
    changes: [
      {
        formula: -4,
        subTarget: "ac",
        modifier: "penalty",
      },
      {
        formula: -4,
        subTarget: "attack",
        modifier: "penalty",
      },
    ],
  },
};

export const conditionTextures = {
  bleed: "systems/lf1/icons/conditions/bleed.png",
  lf1_blind: "systems/lf1/icons/conditions/blind.png",
  confused: "systems/lf1/icons/conditions/confused.png",
  dazzled: "systems/lf1/icons/conditions/dazzled.png",
  lf1_deaf: "systems/lf1/icons/conditions/deaf.png",
  entangled: "systems/lf1/icons/conditions/entangled.png",
  fatigued: "systems/lf1/icons/conditions/fatigued.png",
  exhausted: "systems/lf1/icons/conditions/exhausted.png",
  grappled: "systems/lf1/icons/conditions/grappled.png",
  helpless: "systems/lf1/icons/conditions/helpless.png",
  incorporeal: "systems/lf1/icons/conditions/incorporeal.png",
  invisible: "systems/lf1/icons/conditions/invisible.png",
  paralyzed: "systems/lf1/icons/conditions/paralyzed.png",
  pinned: "systems/lf1/icons/conditions/pinned.png",
  lf1_prone: "systems/lf1/icons/conditions/prone.png",
  sickened: "systems/lf1/icons/conditions/sickened.png",
  staggered: "systems/lf1/icons/conditions/staggered.png",
  stunned: "systems/lf1/icons/conditions/stunned.png",
  shaken: "systems/lf1/icons/conditions/shaken.png",
  frightened: "systems/lf1/icons/conditions/frightened.png",
  panicked: "systems/lf1/icons/conditions/fear.png",
  nauseated: "systems/lf1/icons/conditions/nauseated.png",
  dazed: "systems/lf1/icons/conditions/dazed.png",
  lf1_sleep: "systems/lf1/icons/conditions/sleep.png",
  cowering: "systems/lf1/icons/conditions/screaming.png",
  squeezing: "systems/lf1/icons/conditions/squeezing.png",
};

export const buffTypes = {
  temp: "LF1.Temporary",
  perm: "LF1.Permanent",
  item: "LF1.Item",
  misc: "LF1.Misc",
};

/**
 * Dictionaries of conditional modifier targets, each with a label and sub-categories
 */
export const conditionalTargets = {
  attack: {
    _label: "LF1.AttackRollPlural",
    allAttack: "LF1.All",
    hasteAttack: "LF1.Haste",
    rapidShotAttack: "LF1.RapidShot",
  },
  damage: {
    _label: "LF1.Damage",
    allDamage: "LF1.All",
    hasteDamage: "LF1.Haste",
    rapidShotDamage: "LF1.RapidShot",
  },
  size: {
    _label: "LF1.Size",
  },
  effect: {
    _label: "LF1.Effects",
  },
  misc: {
    _label: "LF1.MiscShort",
  },
};

/**
 * Dictionaries of change/buff targets, each with a label and a category it belongs to,
 * as well as a sort value that determines this buffTarget's priority when Changes are applied.
 */
export const buffTargets = /** @type {const} */ ({
  acpA: { label: "LF1.ACPArmor", category: "misc", sort: 10000 },
  acpS: { label: "LF1.ACPShield", category: "misc", sort: 11000 },
  mDexA: { label: "LF1.MaxDexArmor", category: "misc", sort: 20000 },
  mDexS: { label: "LF1.MaxDexShield", category: "misc", sort: 21000 },
  str: { label: "LF1.AbilityStr", category: "ability", sort: 30000 },
  dex: { label: "LF1.AbilityDex", category: "ability", sort: 31000 },
  con: { label: "LF1.AbilityCon", category: "ability", sort: 32000 },
  int: { label: "LF1.AbilityInt", category: "ability", sort: 33000 },
  wis: { label: "LF1.AbilityWis", category: "ability", sort: 34000 },
  cha: { label: "LF1.AbilityCha", category: "ability", sort: 35000 },
  strMod: { label: "LF1.AbilityStrMod", category: "ability", sort: 40000 },
  dexMod: { label: "LF1.AbilityDexMod", category: "ability", sort: 41000 },
  conMod: { label: "LF1.AbilityConMod", category: "ability", sort: 42000 },
  intMod: { label: "LF1.AbilityIntMod", category: "ability", sort: 43000 },
  wisMod: { label: "LF1.AbilityWisMod", category: "ability", sort: 44000 },
  chaMod: { label: "LF1.AbilityChaMod", category: "ability", sort: 45000 },
  skills: { label: "LF1.BuffTarAllSkills", category: "skills", sort: 50000 },
  unskills: { label: "LF1.BuffTarUntrainedSkills", category: "skills", sort: 100000 },
  carryStr: { label: "LF1.CarryStrength", category: "misc", sort: 60000 },
  carryMult: { label: "LF1.CarryMultiplier", category: "misc", sort: 61000 },
  strSkills: { label: "LF1.BuffTarStrSkills", category: "skills", sort: 70000 },
  dexSkills: { label: "LF1.BuffTarDexSkills", category: "skills", sort: 71000 },
  conSkills: { label: "LF1.BuffTarConSkills", category: "skills", sort: 72000 },
  intSkills: { label: "LF1.BuffTarIntSkills", category: "skills", sort: 73000 },
  wisSkills: { label: "LF1.BuffTarWisSkills", category: "skills", sort: 74000 },
  chaSkills: { label: "LF1.BuffTarChaSkills", category: "skills", sort: 75000 },
  allChecks: { label: "LF1.BuffTarAllAbilityChecks", category: "abilityChecks", sort: 80000 },
  strChecks: { label: "LF1.BuffTarStrChecks", category: "abilityChecks", sort: 81000 },
  dexChecks: { label: "LF1.BuffTarDexChecks", category: "abilityChecks", sort: 82000 },
  conChecks: { label: "LF1.BuffTarConChecks", category: "abilityChecks", sort: 83000 },
  intChecks: { label: "LF1.BuffTarIntChecks", category: "abilityChecks", sort: 84000 },
  wisChecks: { label: "LF1.BuffTarWisChecks", category: "abilityChecks", sort: 85000 },
  chaChecks: { label: "LF1.BuffTarChaChecks", category: "abilityChecks", sort: 86000 },
  landSpeed: { label: "LF1.SpeedLand", category: "speed", sort: 90000 },
  climbSpeed: { label: "LF1.SpeedClimb", category: "speed", sort: 91000 },
  swimSpeed: { label: "LF1.SpeedSwim", category: "speed", sort: 92000 },
  burrowSpeed: { label: "LF1.SpeedBurrow", category: "speed", sort: 93000 },
  flySpeed: { label: "LF1.SpeedFly", category: "speed", sort: 94000 },
  allSpeeds: { label: "LF1.BuffTarAllSpeeds", category: "speed", sort: 95000 },
  ac: { label: "LF1.BuffTarACGeneric", category: "defense", sort: 100000 },
  aac: { label: "LF1.BuffTarACArmor", category: "defense", sort: 101000 },
  sac: { label: "LF1.BuffTarACShield", category: "defense", sort: 102000 },
  nac: { label: "LF1.BuffTarACNatural", category: "defense", sort: 103000 },
  tac: { label: "LF1.BuffTarACTouch", category: "defense", sort: 104000 },
  ffac: { label: "LF1.BuffTarACFlatFooted", category: "defense", sort: 105000 },
  attack: { label: "LF1.BuffTarAllAttackRolls", category: "attack", sort: 110000 },
  bab: { label: "LF1.BAB", category: "attack", sort: 111000 },
  "~attackCore": { label: "", category: "attack", sort: 112000 },
  mattack: { label: "LF1.BuffTarMeleeAttack", category: "attack", sort: 113000 },
  rattack: { label: "LF1.BuffTarRangedAttack", category: "attack", sort: 114000 },
  damage: { label: "LF1.BuffTarAllDamageRolls", category: "damage", sort: 120000 },
  wdamage: { label: "LF1.WeaponDamage", category: "damage", sort: 121000 },
  sdamage: { label: "LF1.SpellDamage", category: "damage", sort: 122000 },
  critConfirm: { label: "LF1.CriticalConfirmation", category: "attack", sort: 130000 },
  allSavingThrows: { label: "LF1.BuffTarAllSavingThrows", category: "savingThrows", sort: 140000 },
  fort: { label: "LF1.SavingThrowFort", category: "savingThrows", sort: 141000 },
  ref: { label: "LF1.SavingThrowRef", category: "savingThrows", sort: 142000 },
  will: { label: "LF1.SavingThrowWill", category: "savingThrows", sort: 143000 },
  cmb: { label: "LF1.CMB", category: "attack", sort: 150000 },
  cmd: { label: "LF1.CMD", category: "defense", sort: 151000 },
  ffcmd: { label: "LF1.CMDFlatFooted", category: "defense", sort: 152000 },
  init: { label: "LF1.Initiative", category: "misc", sort: 160000 },
  mhp: { label: "LF1.HitPoints", category: "health", sort: 170000 },
  wounds: { label: "LF1.Wounds", category: "health", sort: 180000 },
  vigor: { label: "LF1.Vigor", category: "health", sort: 181000 },
  spellResist: { label: "LF1.SpellResistance", category: "defense", sort: 190000 },
  bonusFeats: { label: "LF1.BuffTarBonusFeats", category: "misc", sort: 200000 },
  bonusSkillRanks: { label: "LF1.BuffTarBonusSkillRanks", category: "skills", sort: 210000 },
  concentration: { label: "LF1.Concentration", category: "spell", sort: 220000 },
  cl: { label: "LF1.CasterLevel", category: "spell", sort: 230000 },
});

/**
 * Categories grouping related {@link BuffTarget change targets} in the selector UI.
 */
export const buffTargetCategories = /** @type {const} */ ({
  defense: { label: "LF1.Defense" },
  savingThrows: { label: "LF1.SavingThrowPlural" },
  attack: { label: "LF1.Attack" },
  damage: { label: "LF1.Damage" },
  ability: { label: "LF1.AbilityScore" },
  abilityChecks: { label: "LF1.BuffTarAbilityChecks" },
  health: { label: "LF1.Health" },
  skills: { label: "LF1.Skills" },
  skill: { label: "LF1.BuffTarSpecificSkill" },
  speed: { label: "LF1.Speed" },
  spell: { label: "LF1.BuffTarSpells" },
  misc: { label: "LF1.Misc" },
});

export const contextNoteTargets = {
  attack: { label: "LF1.AttackRollPlural", category: "attacks" },
  effect: { label: "LF1.Effects", category: "attacks" },
  melee: { label: "LF1.Melee", category: "attacks" },
  meleeWeapon: { label: "LF1.MeleeWeapon", category: "attacks" },
  meleeSpell: { label: "LF1.MeleeSpell", category: "attacks" },
  ranged: { label: "LF1.Ranged", category: "attacks" },
  rangedWeapon: { label: "LF1.RangedWeapon", category: "attacks" },
  rangedSpell: { label: "LF1.RangedSpell", category: "attacks" },
  cmb: { label: "LF1.CMB", category: "attacks" },
  allSavingThrows: { label: "LF1.BuffTarAllSavingThrows", category: "savingThrows" },
  fort: { label: "LF1.SavingThrowFort", category: "savingThrows" },
  ref: { label: "LF1.SavingThrowRef", category: "savingThrows" },
  will: { label: "LF1.SavingThrowWill", category: "savingThrows" },
  skills: { label: "LF1.BuffTarAllSkills", category: "skills" },
  strSkills: { label: "LF1.BuffTarStrSkills", category: "skills" },
  dexSkills: { label: "LF1.BuffTarDexSkills", category: "skills" },
  conSkills: { label: "LF1.BuffTarConSkills", category: "skills" },
  intSkills: { label: "LF1.BuffTarIntSkills", category: "skills" },
  wisSkills: { label: "LF1.BuffTarWisSkills", category: "skills" },
  chaSkills: { label: "LF1.BuffTarChaSkills", category: "skills" },
  allChecks: { label: "LF1.BuffTarAllAbilityChecks", category: "abilityChecks" },
  strChecks: { label: "LF1.BuffTarStrChecks", category: "abilityChecks" },
  dexChecks: { label: "LF1.BuffTarDexChecks", category: "abilityChecks" },
  conChecks: { label: "LF1.BuffTarConChecks", category: "abilityChecks" },
  intChecks: { label: "LF1.BuffTarIntChecks", category: "abilityChecks" },
  wisChecks: { label: "LF1.BuffTarWisChecks", category: "abilityChecks" },
  chaChecks: { label: "LF1.BuffTarChaChecks", category: "abilityChecks" },
  spellEffect: { label: "LF1.SpellBuffEffect", category: "spell" },
  concentration: { label: "LF1.Concentration", category: "spell" },
  cl: { label: "LF1.CasterLevel", category: "spell" },
  ac: { label: "LF1.ACNormal", category: "defense" },
  cmd: { label: "LF1.CMD", category: "defense" },
  sr: { label: "LF1.SpellResistance", category: "defense" },
  init: { label: "LF1.Initiative", category: "misc" },
};

export const contextNoteCategories = {
  attacks: { label: "LF1.Attacks" },
  savingThrows: { label: "LF1.SavingThrowPlural" },
  skills: { label: "LF1.Skills" },
  skill: { label: "LF1.BuffTarSpecificSkill" },
  abilityChecks: { label: "LF1.BuffTarAbilityChecks" },
  spell: { label: "LF1.BuffTarSpells" },
  defense: { label: "LF1.Defense" },
  misc: { label: "LF1.Misc" },
};

/**
 * A list of Golarion's languages
 */
export const languages = {
  aboleth: "LF1.LanguageAboleth",
  abyssal: "LF1.LanguageAbyssal",
  aklo: "LF1.LanguageAklo",
  ancientosiriani: "LF1.LanguageAncientOsiriani",
  aquan: "LF1.LanguageAquan",
  auran: "LF1.LanguageAuran",
  azlanti: "LF1.LanguageAzlanti",
  boggard: "LF1.LanguageBoggard",
  celestial: "LF1.LanguageCelestial",
  common: "LF1.LanguageCommon",
  cyclops: "LF1.LanguageCyclops",
  dark: "LF1.LanguageDark",
  draconic: "LF1.LanguageDraconic",
  drowsign: "LF1.LanguageDrowsign",
  druidic: "LF1.LanguageDruidic",
  dwarven: "LF1.LanguageDwarven",
  dziriak: "LF1.LanguageDziriak",
  elven: "LF1.LanguageElven",
  giant: "LF1.LanguageGiant",
  gnoll: "LF1.LanguageGnoll",
  gnome: "LF1.LanguageGnome",
  goblin: "LF1.LanguageGoblin",
  grippli: "LF1.LanguageGrippli",
  halfling: "LF1.LanguageHalfling",
  hallit: "LF1.LanguageHallit",
  ignan: "LF1.LanguageIgnan",
  jistka: "LF1.LanguageJistka",
  infernal: "LF1.LanguageInfernal",
  kelish: "LF1.LanguageKelish",
  necril: "LF1.LanguageNecril",
  orc: "LF1.LanguageOrc",
  orvian: "LF1.LanguageOrvian",
  osiriani: "LF1.LanguageOsiriani",
  polyglot: "LF1.LanguagePolyglot",
  protean: "LF1.LanguageProtean",
  shadowtongue: "LF1.LanguageShadowTongue",
  shoanti: "LF1.LanguageShoanti",
  skald: "LF1.LanguageSkald",
  sphinx: "LF1.LanguageSphinx",
  sylvan: "LF1.LanguageSylvan",
  taldane: "LF1.LanguageTaldane",
  tekritanin: "LF1.LanguageTekritanin",
  tengu: "LF1.LanguageTengu",
  terran: "LF1.LanguageTerran",
  thassilonian: "LF1.LanguageThassilonian",
  tien: "LF1.LanguageTien",
  treant: "LF1.LanguageTreant",
  undercommon: "LF1.LanguageUndercommon",
  varisian: "LF1.LanguageVarisian",
  vegepygmy: "LF1.LanguageVegepygmy",
  vudrani: "LF1.LanguageVudrani",
};

/**
 * Creature types
 */
export const creatureTypes = {
  aberration: "LF1.CreatureTypeAberration",
  animal: "LF1.CreatureTypeAnimal",
  construct: "LF1.CreatureTypeConstruct",
  dragon: "LF1.CreatureTypeDragon",
  fey: "LF1.CreatureTypeFey",
  humanoid: "LF1.CreatureTypeHumanoid",
  magicalBeast: "LF1.CreatureTypeMagicalBeast",
  monstrousHumanoid: "LF1.CreatureTypeMonstrousHumanoid",
  ooze: "LF1.CreatureTypeOoze",
  outsider: "LF1.CreatureTypeOutsider",
  plant: "LF1.CreatureTypePlant",
  undead: "LF1.CreatureTypeUndead",
  vermin: "LF1.CreatureTypeVermin",
};

export const spellRangeFormulas = {
  close: "25 + floor(@cl / 2) * 5",
  medium: "100 + @cl * 10",
  long: "400 + @cl * 40",
};

/**
 * An array containing the damage dice progression for size adjustments
 */
export const sizeDie = [
  "1",
  "1d2",
  "1d3",
  "1d4",
  "1d6",
  "1d8",
  "1d10",
  "2d6",
  "2d8",
  "3d6",
  "3d8",
  "4d6",
  "4d8",
  "6d6",
  "6d8",
  "8d6",
  "8d8",
  "12d6",
  "12d8",
  "16d6",
  "16d8",
];

/**
 * Arrays of Character Level XP Requirements by XP track
 */
// prettier-ignore
export const CHARACTER_EXP_LEVELS =  {
    slow: [
      0,
      3000,
      7500,
      14000,
      23000,
      35000,
      53000,
      77000,
      115000,
      160000,
      235000,
      330000,
      475000,
      665000,
      955000,
      1350000,
      1900000,
      2700000,
      3850000,
      5350000,
    ],
    medium: [
      0,
      2000,
      5000,
      9000,
      15000,
      23000,
      35000,
      51000,
      75000,
      105000,
      155000,
      220000,
      315000,
      445000,
      635000,
      890000,
      1300000,
      1800000,
      2550000,
      3600000,
    ],
    fast: [
      0,
      1300,
      3300,
      6000,
      10000,
      15000,
      23000,
      34000,
      50000,
      71000,
      105000,
      145000,
      210000,
      295000,
      425000,
      600000,
      850000,
      1200000,
      1700000,
      2400000,
    ],
};

/**
 * An array of Challenge Rating XP Levels
 */
// prettier-ignore
export const CR_EXP_LEVELS =  [
    200,
    400,
    600,
    800,
    1200,
    1600,
    2400,
    3200,
    4800,
    6400,
    9600,
    12800,
    19200,
    25600,
    38400,
    51200,
    76800,
    102400,
    153600,
    204800,
    307200,
    409600,
    614400,
    819200,
    1228800,
    1638400,
    2457600,
    3276800,
    4915200,
    6553600,
    9830400,
  ];

export const temporaryRollDataFields = {
  actor: [
    "d20",
    "attackBonus",
    "attackCount",
    "formulaicAttack",
    "damageBonus",
    "pointBlankBonus",
    "rapidShotPenalty",
    "powerAttackBonus",
    "powerAttackPenalty",
    "conditionals",
    "concentrationBonus",
    "formulaBonus",
    "dcBonus",
    "chargeCostBonus",
    "chargeCost",
    "sizeBonus",
    "bonus",
    "critMult",
    "ablMult",
    "ablDamage",
    "cl",
    "sl",
    "classLevel",
    "ablMod",
    "item",
    "action",
    "level",
    "mod",
  ],
};

export const keepItemLinksOnCopy = ["classAssociations"];

export const defaultIcons = {
  items: {
    attack: "icons/svg/explosion.svg",
    buff: "icons/svg/ice-aura.svg",
    class: "icons/svg/paralysis.svg",
    consumable: "icons/svg/tankard.svg",
    container: "icons/svg/barrel.svg",
    equipment: "icons/svg/combat.svg",
    feat: "icons/svg/book.svg",
    loot: "icons/svg/item-bag.svg",
    race: "icons/svg/wing.svg",
    spell: "icons/svg/daze.svg",
    weapon: "icons/svg/sword.svg",
  },
  actors: {
    character: "icons/svg/mystery-man.svg",
    npc: "icons/svg/terror.svg",
    haunt: "icons/svg/stoned.svg",
    basic: "icons/svg/castle.svg",
    trap: "icons/svg/net.svg",
    vehicle: "icons/svg/stone-path.svg",
  },
};
