import * as _lf1 from "./lf1.mjs";

export {};

declare global {
  export import lf1 = _lf1;

  // Document classes
  export import ActorPF = lf1.documents.actor.ActorPF;
  export import ItemPF = lf1.documents.item.ItemPF;
  export import ChatMessagePF = lf1.documents.ChatMessagePF;
  export import TokenDocumentPF = lf1.documents.TokenDocumentPF;
  export import ActiveEffectPF = lf1.documents.ActiveEffectPF;
  export import CombatPF = lf1.documents.CombatPF;

  // Component classes
  export import ItemChange = lf1.components.ItemChange;
  export import ItemAction = lf1.components.ItemAction;

  // Canvas classes
  export import TokenPF = lf1.canvas.TokenPF;
  export import MeasuredTemplatePF = lf1.canvas.MeasuredTemplatePF;

  // Roll-related classes
  export import RollPF = lf1.dice.RollPF;

  // UI classes
  export import TooltipPF = lf1.applications.TooltipPF;

  interface DocumentClassConfig {
    // Documents
    // Actor and Item types are not technically correct, as they are not the base classes.
    // They are however the base class for system-specific classes, and provide easier access to system-specific autocomplete.
    Actor: typeof lf1.documents.actor.ActorPF;
    Item: typeof lf1.documents.item.ItemPF;
    ChatMessage: typeof lf1.documents.ChatMessagePF;
    TokenDocument: typeof lf1.documents.TokenDocumentPF;
  }

  interface LenientGlobalVariableTypes {
    game: unknown; // the type doesn't matter
    quench: unknown;
  }
}
